/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Array.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/16 13:50:07 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/16 13:50:08 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef ARRAY_HPP
#define ARRAY_HPP

#include <iostream>

template <class T>
class Array {

public:

	Array<T>() {
		_var = new T;
		_size = 0;
	}

	Array<T>(unsigned int n) {
		_var = new T[n];
		_size = n;
		for (unsigned int i = 0; i < _size; i++)
			_var[i] = T();
	}

	T & operator[](unsigned int index) {
		if (index >= _size)
			throw std::exception();
		return (_var[index]);
	}

	T const & operator[](T const index) const {
		if (index >= _size)
			throw std::exception();
		return (_var[index]);
	}

	Array<T>(const Array & copy) {
		*this = copy;
	}

	Array & operator=(const Array & rhs) {
		if (this == &rhs)
			return (*this);
		_size = rhs._size;
		if (rhs._size == 0)
			_var = new T;
		else
			_var = new T[_size];
		for (unsigned int i = 0; i < _size; i++)
			_var[i] = rhs._var[i];
		return (*this);
	}

	~Array() {
		if (_size == 0)
			delete _var;
		else
			delete [] _var;
	}

	unsigned int getSize() const {
		return (_size);
	}

private:

	T		*_var;
	unsigned int	_size;

};

#endif
