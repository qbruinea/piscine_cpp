/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/16 13:50:09 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/16 13:50:10 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Array.hpp"

int main(void)
{
	std::string const str0 = "salut";
	std::string const str1 = "les";
	std::string const str2 = "terriens";


	Array<std::string> test(3);
	test[0] = str0;
	test[1] = str1;
	test[2] = str2;

	Array<std::string> test2(test);

	Array<std::string> test3;
	test3 = test2;

	try {
		std::cout << test[0] << std::endl;
		std::cout << test[1] << std::endl;
		std::cout << test[2] << std::endl;
		//std::cout << test[3] << std::endl;

		std::cout << "\nCopy" << std::endl;
		std::cout << test2[0] << std::endl;
		std::cout << test2[1] << std::endl;
		std::cout << test2[2] << std::endl;

		std::cout << "\noperator" << std::endl;
		std::cout << test3[0] << std::endl;
		std::cout << test3[1] << std::endl;
		std::cout << test3[2] << std::endl;
	}
	catch(std::exception &e) {
		std::cout << "Index out of range\n";
	}
	std::cout << "\nSize" << std::endl;
	std::cout << test.getSize() << std::endl;
	std::cout << test2.getSize() << std::endl;
	std::cout << test3.getSize() << std::endl;
	return (0);
}