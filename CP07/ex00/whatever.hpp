/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   whatever.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/16 13:50:01 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/16 13:50:02 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef WHATEVER_HPP
#define WHATEVER_HPP

#include <iostream>

template <typename T>
T	max(T & a, T & b) {
	if (a > b)
		return (a);
	return (b);
}

template <typename T>
T	const max(const T & a, const T & b) {
	if (a > b)
		return (a);
	return (b);
}

template <typename T>
T	min(T & a, T & b) {
	if (a < b)
		return (a);
	return (b);
}

template <typename T>
T	const min(const T & a, const T & b) {
	if (a < b)
		return (a);
	return (b);
}

template <typename T>
void swap(T & a, T & b) {
	T	tmp = a;
	a = b;
	b = tmp;
}


#endif
