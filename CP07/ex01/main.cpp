/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/16 13:50:04 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/16 13:50:05 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "iter.hpp"

template <typename T>
void print(T const & rhs) {
	std::cout << rhs << std::endl;
}

template <typename T>
void addOne(T & t) {
	t++;
}

int main(void)
{
	int	number[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
	std::string str[] = {"Salut", "a", "toi"};

	::iter(number, 10, print);
	std::cout << std::endl;
	::iter(number, 10, addOne);
	::iter(number, 10, print);
	std::cout << std::endl;
	::iter(str, 3, print);

	return (0);
}

