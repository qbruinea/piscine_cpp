/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   iter.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/16 13:50:03 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/16 13:50:04 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef ITER_HPP
#define ITER_HPP

#include <iostream>

template <typename T, typename S>
void	iter(T * array, S lenght, void (*func)(T &)) {
	for (S i = 0; i < lenght; i++)
		func((array)[i]);
}

template <typename T, typename S>
void	iter(T const * array, S lenght, void (*func)(T const &)) {
	for (S i = 0; i < lenght; i++)
		func((array)[i]);
}

#endif
