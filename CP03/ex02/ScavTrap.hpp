/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/26 16:30:25 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/26 16:30:29 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCAVTRAP_HPP
# define SCAVTRAP_HPP

#include "ClapTrap.hpp"

class ScavTrap : public ClapTrap {

public:

	ScavTrap(const std::string name);
	ScavTrap(ScavTrap const & copy);
	~ScavTrap();

	ScavTrap	&operator=(const ScavTrap & rhs);

	void	challengeNewcomer(std::string const & target);

private:

	ScavTrap();

};

#endif