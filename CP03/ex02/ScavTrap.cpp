/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/26 16:30:23 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/26 16:30:29 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "ScavTrap.hpp"

ScavTrap::ScavTrap(const std::string name) : ClapTrap(100, 100, 50, 50, 1, 20, 15, 3, name) {
	std::cout << "SCAV-TP " << name << " is here to serve you sir." << std::endl;

	return;
}

ScavTrap::ScavTrap(const ScavTrap &copy) : ClapTrap(copy) {
	return;
}

ScavTrap::~ScavTrap() {
	std::cout << "SCAV-TP " << this->_name << " finish his duty." << std::endl;

	return;
}

void ScavTrap::challengeNewcomer(const std::string &target) {
	//std::cout << this->_hit_point << std::endl;
	if (this->_hit_point == 0)
	{
		std::cout << "SCAV-TP " << this->_name << " can't challenge anything. HE'S GODDAM DEAD !" << std::endl;
		return;
	}

	std::string	nameChallenge[5] = {
			" to chubby bunny !",
			" to lick his elbow.",
			" to vaccum the beach.",
			" to run in the forest blindfolded.",
			" to dry his hair in the microwave"
	};
	if (this->_energy_points < 25)
	{
		std::cout << "SCAV-TP " << this->_name << " has not the energy to challenge anyone." << std::endl;
		return;
	}
	if (this->_energy_points >= 25)
	{
		std::cout << "SCAV-TP " << this->_name << " challenge " << target << nameChallenge[rand() % 5] << std::endl;
		this->_energy_points -= 25;
		return;
	}
	return;
}

ScavTrap & ScavTrap::operator=(const ScavTrap & rhs) {
	if (this == &rhs)
		return (*this);
	ClapTrap::operator=(rhs);
	return (*this);
}
