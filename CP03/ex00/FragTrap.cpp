/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/26 16:27:41 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/26 16:28:12 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"

FragTrap::FragTrap(const std::string name) :
_hit_point(100), _max_hits_points(100), _energy_points(100), _max_energy_points(100), _level(1),
_melee_attack_damage(30), _ranged_attack_damage(20), _armor_damage_reduction(5), _name(name) {

	std::cout << "Welcome " << name << " to our simulation program." << std::endl;

	return;
}

FragTrap::FragTrap(const FragTrap &copy) {
	*this = copy;

	return;
}

FragTrap::~FragTrap() {
	std::cout << "Simulation termined. Hope see you soon !" << std::endl;

	return;
}

void FragTrap::rangedAttack(const std::string &target) {
	if (this->_hit_point == 0)
	{
		std::cout << "FR4G-TP " << this->_name << " can't attack, his HP are at " << this->_hit_point << ".You should maybe repair him." << std::endl;
		return;
	}

	std::cout << "FR4G-TP " << this->_name << " attacks " << target << " at range, causing ";
	std::cout << this->_ranged_attack_damage << " points of damage!" << std::endl;
}

void FragTrap::meleeAttack(const std::string &target) {
	if (this->_hit_point == 0)
	{
		std::cout << "FR4G-TP " << this->_name << " can't attack, his HP are at " << this->_hit_point << ".You should maybe repair him." << std::endl;
		return;
	}

	std::cout << "FR4G-TP " << this->_name << " attacks " << target << " at melee, causing ";
	std::cout << this->_melee_attack_damage << " points of damage!" << std::endl;
}

void FragTrap::takeDamage(unsigned int amount) {
	unsigned int	real_dmg = this->_max_hits_points + this->_armor_damage_reduction;

	if ((int)amount < 0)
	{
		std::cout << "You want inflict negativ damage ? Really ?!" << std::endl;
		return;
	}
	if (this->_hit_point == 0)
	{
		std::cout << "You can't attack a dead robot. Is HP is already at " << this->_hit_point << std::endl;
		return;
	}
	if (amount >= real_dmg)
	{
		this->_hit_point = 0;
		std::cout << "FR4G-TP take too much damge, IT'S OVER " << real_dmg << " !!!!!!!! FR4G-TP Must be repaired !" << std::endl;
		return;
	}
	if (amount <= (unsigned int)this->_armor_damage_reduction)
		real_dmg = 0;
	else
		real_dmg = amount - this->_armor_damage_reduction;
	this->_hit_point -= real_dmg;
	if (this->_hit_point < 0)
	{
		this->_hit_point = 0;
		std::cout << "FR4G-TP hp drop down to " << this->_hit_point << ", he must be repaired !" << std::endl;
		return;
	}
	std::cout << "FR4G-TP " << this->_name << " has been attacked, he take " << amount << " but take " << real_dmg << " thank to his armor." << std::endl;
}

void FragTrap::beRepaired(unsigned int amount) {
	if ((int)amount < 0)
	{
		std::cout << "How could repair with a negativ value ???" << std::endl;
		return;
	}
	if (this->_hit_point == this->_max_hits_points)
	{
		std::cout << "Why would you repair a brand new shiny robot like me ?" << std::endl;
		return;
	}
	if (this->_hit_point + (int)amount > this->_max_hits_points)
	{
		this->_hit_point = this->_max_hits_points;
		std::cout << "FR4G-TP is fully restored, well done !" << std::endl;
		return;
	}
	this->_hit_point += amount;
	std::cout << "FR4G-TP gain " << amount << " hp. He's now at " << this->_hit_point << std::endl;

}

void FragTrap::vaulthunter_dot_exe(const std::string &target) {
	if (this->_hit_point == 0)
	{
		std::cout << "FR4G-TP " << this->_name << " can't attack, his HP are at " << this->_hit_point << ".You should maybe repair him." << std::endl;
		return;
	}

	std::string	nameAttack[5] =
			{"SUPER GALAXY GIGA DRILL BREAKER !!!",
			"KONO DIO DA ! MUDA MUDA MUDA MUDA MUDA MUDA MUDA MUDAAAAAAAAAAA !!!",
			"Kyûjû kyû no Te.",
			"GALICK GUN !",
			"Greater pyroblast."};
	std::string descriptionAttack[5] = {
			" He broke the entire space and time universe !",
			" He rapidly hit with his fist, dealing 100 damage !",
			" He hit with the ultimate god power fist, dealing 500 damage !",
			" He fire a powerful purple ray, dealing 50 damage !",
			" He fire a retarded spamming mage attack, dealing half life of his target."};

	int random = rand() % 5;

	if (this->_energy_points < 25)
	{
		std::cout << "FR4G-TP " << this->_name << " has not enough energy ! He just moan something unearable." << std::endl;
		return;
	}
	if (this->_energy_points >= 25)
	{
		std::cout << "FR4G-TP " << this->_name << " attack " << target << " with his special abilitie ! " << nameAttack[random] << descriptionAttack[random] << std::endl;
		this->_energy_points -= 25;
		return;
	}
	return;
}

FragTrap & FragTrap::operator=(const FragTrap & rhs) {
	if (this == &rhs)
		return (*this);
	this->_hit_point = rhs._hit_point;
	this->_max_hits_points = rhs._max_hits_points;
	this->_energy_points = rhs._energy_points;
	this->_max_energy_points = rhs._max_energy_points;
	this->_level = rhs._level;
	this->_name = rhs._name;
	this->_melee_attack_damage = rhs._melee_attack_damage;
	this->_ranged_attack_damage = rhs._ranged_attack_damage;
	this->_armor_damage_reduction = rhs._armor_damage_reduction;
	return (*this);
}