/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/26 16:27:46 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/26 16:29:25 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"

int	main()
{
	srand(time(NULL));

	FragTrap first("Jean-Eude");

	std::cout << std::endl;

	first.rangedAttack("Sarah Connor");
	first.takeDamage(30);
	first.meleeAttack("Barbi");
	first.takeDamage(30);
	first.beRepaired(66);
	first.vaulthunter_dot_exe("Jean-Castex");
	first.takeDamage(66);
	first.beRepaired(66);
	first.vaulthunter_dot_exe("a squirrel");
	first.takeDamage(1);
	first.beRepaired(1);
	first.takeDamage(2147483649);
	first.vaulthunter_dot_exe("the sun");
	first.rangedAttack("the tooth fairy");
	first.meleeAttack("Chabal");
	first.beRepaired(-1);
	first.beRepaired(999);
	first.vaulthunter_dot_exe("Josephine");
	first.vaulthunter_dot_exe("Zelda");
	first.takeDamage(-1);

	std::cout << std::endl;

	return (0);
}