/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperTrap.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/26 16:31:44 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/26 16:31:49 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "SuperTrap.hpp"

SuperTrap::SuperTrap(const std::string name) :
ClapTrap(100, 100, 120, 120, 1, 60, 20, 5, name),
FragTrap(name), NinjaTrap(name)
{
	std::cout << "SUPER TRAP " << name << " HAS ARRIVED TO SAVE THE WORLD !!" << std::endl;

	return;
}

SuperTrap::SuperTrap(const SuperTrap &copy) :
ClapTrap(copy._hit_point, copy._max_hits_points, copy._energy_points, copy._max_energy_points, copy._level, copy._melee_attack_damage, copy._ranged_attack_damage, copy._armor_damage_reduction, copy._name),
FragTrap(copy._name), NinjaTrap(copy._name) {
	return;
}

SuperTrap::~SuperTrap() {
	std::cout << "NOOOOOOOO ! " << this->_name << " HAS BEEN DEFEATED !!!" << std::endl;

	return;
}

SuperTrap & SuperTrap::operator=(const SuperTrap &rhs) {
	if (this == &rhs)
		return (*this);
	ClapTrap::operator=(rhs);
	return (*this);
}