/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/26 16:31:32 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/26 16:31:49 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRAGTRAP_HPP
# define FRAGTRAP_HPP

#include "ClapTrap.hpp"

class FragTrap : virtual public ClapTrap {

public:

	FragTrap(const std::string name);
	FragTrap(FragTrap const & copy);
	~FragTrap();

	void rangedAttack(std::string const & target);

	FragTrap	&operator=(const FragTrap & rhs);

	void	vaulthunter_dot_exe(std::string const & target);

private:

	FragTrap();


};

#endif