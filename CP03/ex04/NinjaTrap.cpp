/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NinjaTrap.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/26 16:31:00 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/26 16:31:08 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "NinjaTrap.hpp"

NinjaTrap::NinjaTrap(const std::string name) : ClapTrap(60, 60, 120, 120, 1, 60, 5, 0, name){
	std::cout << "NINJA-TP " << name << " just appear from da shadow !" <<std::endl;
}

NinjaTrap::NinjaTrap(const NinjaTrap &copy) : ClapTrap(copy) {
	return;
}

NinjaTrap::~NinjaTrap() {
	std::cout << "NINJA-TP " << this->_name << " go back in da dark world. Have fun buddy !" << std::endl;

	return;
}

void NinjaTrap::ninjaShoebox(const ScavTrap &target) {
	if (this->_hit_point == 0)
	{
		std::cout << "NINJA-TP " << this->_name << " have 0 HP. Maybe you should repair him instead of doing nonsense" << std::endl;
		return;
	}
	if (this->_energy_points < 20)
	{
		std::cout << "NINJA-TP " << this->_name << " has not energy to make his special ninjaShoebox on the SCAV-TP " << target.getName() << std::endl;
		return;
	}
	if (this->_energy_points >= 20)
	{
		std::cout << "NINJA-TP " << this->_name << " point " << target.getName() << " and accept all his challenge ! But first he throw him a shuriken, dealing " << this->_ranged_attack_damage << " points of damage." << std::endl;
		this->_energy_points -= 20;
	}
	return;
}

void NinjaTrap::ninjaShoebox(const ClapTrap &target) {
	if (this->_hit_point == 0)
	{
		std::cout << "NINJA-TP " << this->_name << " have 0 HP. Maybe you should repair him instead of doing nonsense" << std::endl;
		return;
	}
	if (this->_energy_points < 20)
	{
		std::cout << "NINJA-TP " << this->_name << " has not energy to make his special ninjaShoebox on the CLAP-TP " << target.getName() << std::endl;
		return;
	}
	if (this->_energy_points >= 20)
	{
		std::cout << "NINJA-TP " << this->_name << " laugh at " << target.getName() << ". What's that unevolve robot ?! I will crush you ! OMAE WA MOU SHINDEIRU !" << std::endl;
		std::cout << target.getName() << " : NANI ?!?!" << std::endl;
		this->_energy_points -= 20;
	}
	return;
}

void NinjaTrap::ninjaShoebox(const FragTrap &target) {
	if (this->_hit_point == 0)
	{
		std::cout << "NINJA-TP " << this->_name << " have 0 HP. Maybe you should repair him instead of doing nonsense" << std::endl;
		return;
	}
	if (this->_energy_points < 20)
	{
		std::cout << "NINJA-TP " << this->_name << " has not energy to make his special ninjaShoebox on the FR4G-TP " << target.getName() << std::endl;
		return;
	}
	if (this->_energy_points >= 20)
	{
		std::cout << "NINJA-TP " << this->_name << " spotted the FR4G-TP " << target.getName() << " and decide to face him." << std::endl;
		this->meleeAttack(target.getName());
		std::cout << "Here come on other one !" << std::endl;
		this->meleeAttack(target.getName());
		this->_energy_points -= 20;
	}
	return;
}

void NinjaTrap::ninjaShoebox(const NinjaTrap &target) {
	if (this->_hit_point == 0)
	{
		std::cout << "NINJA-TP " << this->_name << " have 0 HP. Maybe you should repair him instead of doing nonsense" << std::endl;
		return;
	}
	std::cout << "NINJA-TP " << this->_name << " meet an another robot like him." << "He decide to take a cup of oil with his new friend " << target.getName() << std::endl;
	this->beRepaired(5);
	
	return;
}

void NinjaTrap::meleeAttack(const std::string &target) {
	if (this->_hit_point == 0)
	{
		std::cout << "HE CAN'T DO THIS, CUZ X_X !!!" << std::endl;
		return;
	}
	std::cout << this->_name << " attack " << target << " with a BIG MELEE ATTACK ! Causing " << this->_melee_attack_damage << " damage !" << std::endl;
	return;
}

NinjaTrap & NinjaTrap::operator=(const NinjaTrap &rhs) {
	if (this == &rhs)
		return (*this);
	ClapTrap::operator=(rhs);
	return (*this);
}