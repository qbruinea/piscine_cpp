/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/26 16:31:28 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/26 16:31:49 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef CLAPTRAP_HPP
# define CLAPTRAP_HPP

#include <iostream>

class ClapTrap {

public:


	ClapTrap(ClapTrap const & copy);
	ClapTrap(int hp, int max_hp, int ep, int max_ep, int lvl, int melee_dmg, int range_dmg, int armor, std::string name);
	~ClapTrap();

	ClapTrap	&operator=(const ClapTrap & rhs);

	void	rangedAttack(std::string const & target);
	void	meleeAttack(std::string const & target);
	void	takeDamage(unsigned int amount);
	void	beRepaired(unsigned int amount);

	const std::string &getName() const ;


protected:

	ClapTrap();

	int			_hit_point;
	int			_max_hits_points;
	int			_energy_points;
	int			_max_energy_points;
	int			_level;
	int			_melee_attack_damage;
	int			_ranged_attack_damage;
	int			_armor_damage_reduction;
	std::string	_name;

};

#endif