/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/26 16:31:34 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/26 16:31:49 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "ScavTrap.hpp"
#include "FragTrap.hpp"
#include "NinjaTrap.hpp"
#include "SuperTrap.hpp"

int main()
{
	srand(time(NULL));

	FragTrap	first("Jean-Eude");
	ScavTrap	second("Alfred");
	NinjaTrap	third("Donatello");
	NinjaTrap	fourth("Leonardo");
	ClapTrap	fifth(10, 10, 0, 0, 1, 2, 1, 0, "Trash");
	SuperTrap	super("Ackbar");

	std::cout << std::endl;

	super.vaulthunter_dot_exe("Vador");
	super.ninjaShoebox(first);
	super.ninjaShoebox(second);
	super.ninjaShoebox(third);
	super.rangedAttack("a bird");
	super.meleeAttack("a bear");
	super.takeDamage(999);
	super.vaulthunter_dot_exe("Vador");
	super.ninjaShoebox(first);
	super.ninjaShoebox(second);
	super.ninjaShoebox(fifth);
	super.rangedAttack("a bird");
	super.meleeAttack("a bear");
	super.beRepaired(99);


	std::cout << std::endl;

	return (0);
}