/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NinjaTrap.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/26 16:31:02 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/26 16:31:08 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef NINJATRAP_HPP
# define NINJATRAP_HPP

#include "FragTrap.hpp"
#include "ScavTrap.hpp"

class NinjaTrap : virtual public ClapTrap {

public:

	NinjaTrap(const std::string name);
	NinjaTrap(NinjaTrap const & copy);
	~NinjaTrap();

	NinjaTrap	&operator=(const NinjaTrap & rhs);

	void	ninjaShoebox(const ScavTrap & target);
	void	ninjaShoebox(const FragTrap & target);
	void	ninjaShoebox(const NinjaTrap & target);
	void	ninjaShoebox(const ClapTrap & target);

	void	meleeAttack(std::string const & target);

private:

	NinjaTrap();

};

#endif