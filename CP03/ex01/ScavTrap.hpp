/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/26 16:28:31 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/26 16:29:05 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCAVTRAP_HPP
# define SCAVTRAP_HPP

#include <iostream>

class ScavTrap {

public:

	ScavTrap(const std::string name);
	ScavTrap(ScavTrap const & copy);
	~ScavTrap();

	ScavTrap	&operator=(const ScavTrap & rhs);

	void	rangedAttack(std::string const & target);
	void	meleeAttack(std::string const & target);
	void	takeDamage(unsigned int amount);
	void	beRepaired(unsigned int amount);

	void	challengeNewcomer(std::string const & target);

private:

	ScavTrap();

	int			_hit_point;
	int			_max_hits_points;
	int			_energy_points;
	int			_max_energy_points;
	int			_level;
	int			_melee_attack_damage;
	int			_ranged_attack_damage;
	int			_armor_damage_reduction;
	std::string	_name;
};

#endif