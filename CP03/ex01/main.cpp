/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/26 16:29:04 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/26 16:29:05 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "ScavTrap.hpp"
#include "FragTrap.hpp"

int main()
{
	srand(time(NULL));

	FragTrap	first("Jean-Eude");
	ScavTrap	second("Alfred");

	std::cout << "\nFRAGTRAP\n" << std::endl;
	first.rangedAttack("Sarah Connor");
	first.takeDamage(30);
	first.meleeAttack("Barbi");
	first.takeDamage(30);
	first.beRepaired(66);
	first.vaulthunter_dot_exe("Jean-Castex");
	first.takeDamage(66);
	first.beRepaired(66);
	first.vaulthunter_dot_exe("a squirrel");
	first.takeDamage(1);
	first.beRepaired(1);
	first.takeDamage(666);
	first.vaulthunter_dot_exe("the sun");
	first.rangedAttack("the tooth fairy");
	first.meleeAttack("Chabal");
	first.beRepaired(-1);
	first.beRepaired(999);
	first.vaulthunter_dot_exe("Josephine");
	first.vaulthunter_dot_exe("Zelda");
	first.takeDamage(-1);

	std::cout << "\nSCAVTRAP\n" << std::endl;
	
	second.rangedAttack("Denise");
	second.takeDamage(12);
	second.meleeAttack("Thanos");
	second.takeDamage(3);
	second.beRepaired(66);
	second.takeDamage(66);
	second.beRepaired(66);
	second.takeDamage(4);
	second.beRepaired(4);
	second.takeDamage(666);
	second.rangedAttack("Teemo");
	second.meleeAttack("the pope");
	second.challengeNewcomer("your mom");
	second.beRepaired(-1);
	second.beRepaired(999);
	second.takeDamage(-1);
	second.challengeNewcomer("Dora");
	second.challengeNewcomer("Chipper");
	second.challengeNewcomer("the Dora's backpack");

	std::cout << std::endl;

	return (0);
}