/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/26 16:28:29 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/26 16:29:05 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "ScavTrap.hpp"

ScavTrap::ScavTrap(const std::string name) :
_hit_point(100), _max_hits_points(100), _energy_points(50), _max_energy_points(50), _level(1),
_melee_attack_damage(20), _ranged_attack_damage(15), _armor_damage_reduction(3), _name(name) {
	std::cout << "SCAV-TP " << name << " is here to serve you sir." << std::endl;

	return;
}

ScavTrap::ScavTrap(const ScavTrap &copy) {
	*this = copy;

	return;
}

ScavTrap::~ScavTrap() {
	std::cout << "SCAV-TP " << this->_name << " finish his duty." << std::endl;

	return;
}

void ScavTrap::rangedAttack(const std::string &target) {
	if (this->_hit_point == 0)
	{
		std::cout << "SCAV-TP " << this->_name << " can't attack, his HP are at " << this->_hit_point << ".You should maybe repair him." << std::endl;
		return;
	}

	std::cout << "SCAV-TP " << this->_name << " attacks " << target << " at range, causing ";
	std::cout << this->_ranged_attack_damage << " points of damage!" << std::endl;
}

void ScavTrap::meleeAttack(const std::string &target) {
	if (this->_hit_point == 0)
	{
		std::cout << "SCAV-TP " << this->_name << " can't attack, his HP are at " << this->_hit_point << ".You should maybe repair him." << std::endl;
		return;
	}

	std::cout << "SCAV-TP " << this->_name << " attacks " << target << " at melee, causing ";
	std::cout << this->_melee_attack_damage << " points of damage!" << std::endl;
}

void ScavTrap::takeDamage(unsigned int amount) {
	unsigned int	real_dmg = this->_max_hits_points + this->_armor_damage_reduction;

	if ((int)amount < 0)
	{
		std::cout << "You want inflict negativ damage ? Really ?!" << std::endl;
		return;
	}
	if (this->_hit_point == 0)
	{
		std::cout << "You can't attack a dead robot. Is HP is already at " << this->_hit_point << std::endl;
		return;
	}
	if (amount >= real_dmg)
	{
		this->_hit_point = 0;
		std::cout << "SCAV-TP take too much damge, IT'S OVER " << real_dmg << " !!!!!!!! SCAV-TP Must be repaired !" << std::endl;
		return;
	}
	if (amount <= (unsigned int)this->_armor_damage_reduction)
		real_dmg = 0;
	else
		real_dmg = amount - this->_armor_damage_reduction;
	this->_hit_point -= real_dmg;
	if (this->_hit_point < 0)
	{
		this->_hit_point = 0;
		std::cout << "SCAV-TP hp drop down to " << this->_hit_point << ", he must be repaired !" << std::endl;
		return;
	}
	std::cout << "SCAV-TP " << this->_name << " has been attacked, he take " << amount << " but take " << real_dmg << " thank to his armor." << std::endl;
}

void ScavTrap::beRepaired(unsigned int amount) {
	if ((int)amount < 0)
	{
		std::cout << "How could repair with a negativ value ???" << std::endl;
		return;
	}
	if (this->_hit_point == this->_max_hits_points)
	{
		std::cout << "Why would you repair a brand new shiny robot like me ?" << std::endl;
		return;
	}
	if (this->_hit_point + (int)amount > this->_max_hits_points)
	{
		this->_hit_point = this->_max_hits_points;
		std::cout << "SCAV-TP is fully restored, well done !" << std::endl;
		return;
	}
	this->_hit_point += amount;
	std::cout << "SCAV-TP gain " << amount << " hp. He's now at " << this->_hit_point << std::endl;

}

void ScavTrap::challengeNewcomer(const std::string &target) {
	if (this->_hit_point == 0)
	{
		std::cout << "SCAV-TP " << this->_name << " can't challenge anything. HE'S GODDAM DEAD !" << std::endl;
		return;
	}

	std::string	nameChallenge[5] = {
			" to chubby bunny !",
			" to lick his elbow.",
			" to vaccum the beach.",
			" to run in the forest blindfolded.",
			" to dry his hair in the microwave"
	};
	if (this->_energy_points < 25)
	{
		std::cout << "SCAV-TP " << this->_name << " has not the energy to challenge anyone." << std::endl;
		return;
	}
	if (this->_energy_points >= 25)
	{
		std::cout << "SCAV-TP " << this->_name << " challenge " << target << nameChallenge[rand() % 5] << std::endl;
		this->_energy_points -= 25;
		return;
	}
	return;
}

ScavTrap & ScavTrap::operator=(const ScavTrap & rhs) {
	if (this == &rhs)
		return (*this);
	this->_hit_point = rhs._hit_point;
	this->_max_hits_points = rhs._max_hits_points;
	this->_energy_points = rhs._energy_points;
	this->_max_energy_points = rhs._max_energy_points;
	this->_level = rhs._level;
	this->_name = rhs._name;
	this->_melee_attack_damage = rhs._melee_attack_damage;
	this->_ranged_attack_damage = rhs._ranged_attack_damage;
	this->_armor_damage_reduction = rhs._armor_damage_reduction;
	return (*this);
}
