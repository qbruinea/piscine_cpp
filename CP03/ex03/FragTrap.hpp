/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/26 16:30:55 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/26 16:31:08 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRAGTRAP_HPP
# define FRAGTRAP_HPP

#include "ClapTrap.hpp"

class FragTrap : public ClapTrap {

public:

	FragTrap(const std::string name);
	FragTrap(FragTrap const & copy);
	~FragTrap();

	FragTrap	&operator=(const FragTrap & rhs);

	void	vaulthunter_dot_exe(std::string const & target);

private:

	FragTrap();


};

#endif