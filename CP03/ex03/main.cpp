/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/26 16:30:58 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/26 16:31:08 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "ScavTrap.hpp"
#include "FragTrap.hpp"
#include "NinjaTrap.hpp"

int main()
{
	srand(time(NULL));

	FragTrap	first("Jean-Eude");
	ScavTrap	second("Alfred");
	NinjaTrap	third("Donatello");
	NinjaTrap	fourth("Leonardo");
	ClapTrap	fifth(10, 10, 0, 0, 1, 2, 1, 0, "Trash");

	std::cout << "\nFRAGTRAP\n" << std::endl;
	first.rangedAttack("Sarah Connor");
	first.takeDamage(30);
	first.meleeAttack("Barbi");
	first.takeDamage(30);
	first.beRepaired(66);
	first.vaulthunter_dot_exe("Jean-Castex");
	first.takeDamage(66);
	first.beRepaired(66);
	first.vaulthunter_dot_exe("a squirrel");
	first.takeDamage(1);
	first.beRepaired(1);
	first.takeDamage(666);
	first.vaulthunter_dot_exe("the sun");
	first.rangedAttack("the tooth fairy");
	first.meleeAttack("Chabal");
	first.beRepaired(-1);
	first.beRepaired(999);
	first.vaulthunter_dot_exe("Josephine");
	first.vaulthunter_dot_exe("Zelda");
	first.takeDamage(-1);

	std::cout << "\nSCAVTRAP\n" << std::endl;


	second.rangedAttack("Denise");
	second.takeDamage(12);
	second.meleeAttack("Thanos");
	second.takeDamage(3);
	second.beRepaired(66);
	second.takeDamage(66);
	second.beRepaired(66);
	second.takeDamage(4);
	second.beRepaired(4);
	second.takeDamage(666);
	second.beRepaired(360000);
	second.rangedAttack("Teemo");
	second.meleeAttack("the pope");
	second.challengeNewcomer("your dad");
	second.beRepaired(-1);
	second.beRepaired(999);
	second.takeDamage(-1);
	second.challengeNewcomer("Dora");
	second.challengeNewcomer("Chipper");
	second.challengeNewcomer("the Dora's backpack");

	std::cout << "\nNINJATRAP\n" << std::endl;


	third.rangedAttack("Freezer");
	third.meleeAttack("Carapuce");
	third.ninjaShoebox(second);
	third.ninjaShoebox(fifth);
	third.ninjaShoebox(first);
	third.takeDamage(5);
	third.ninjaShoebox(fourth);
	third.takeDamage(999);
	third.rangedAttack("Freezer");
	third.meleeAttack("Carapuce");
	third.ninjaShoebox(second);
	third.ninjaShoebox(fifth);
	third.ninjaShoebox(first);
	third.ninjaShoebox(fourth);
	third.beRepaired(55);
	third.beRepaired(-999);
	third.takeDamage(-314156535);

	std::cout << std::endl;

	return (0);
}