/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/26 16:30:53 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/26 16:31:08 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"

FragTrap::FragTrap(const std::string name) : ClapTrap(100, 100, 100, 100, 1, 30, 20, 5, name) {

	std::cout << "Welcome " << name << " to our simulation program." << std::endl;

	return;
}

FragTrap::FragTrap(const FragTrap &copy) : ClapTrap(copy) {
	return;
}

FragTrap::~FragTrap() {
	std::cout << "Simulation termined. Hope see you soon !" << std::endl;

	return;
}

void FragTrap::vaulthunter_dot_exe(const std::string &target) {
	if (this->_hit_point == 0)
	{
		std::cout << "FR4G-TP " << this->_name << " can't attack, his HP are at " << this->_hit_point << ".You should maybe repair him." << std::endl;
		return;
	}

	std::string	nameAttack[5] = {
			"SUPER GALAXY GIGA DRILL BREAKER !!!",
			"KONO DIO DA ! MUDA MUDA MUDA MUDA MUDA MUDA MUDA MUDAAAAAAAAAAA !!!",
			"Kyûjû kyû no Te.",
			"GALICK GUN !",
			"Greater pyroblast."};
	std::string descriptionAttack[5] = {
			" He broke the entire space and time universe !",
			" He rapidly hit with his fist, dealing 100 damage !",
			" He hit with the ultimate god power fist, dealing 500 damage !",
			" He fire a powerful purple ray, dealing 50 damage !",
			" He fire a retarded spamming mage attack, dealing half life of his target."};

	int random = rand() % 5;

	if (this->_energy_points < 25)
	{
		std::cout << "FR4G-TP " << this->_name << " has not enough energy ! He just moan something unearable." << std::endl;
		return;
	}
	if (this->_energy_points >= 25)
	{
		std::cout << "FR4G-TP " << this->_name << " attack " << target << " with his special abilitie ! " << nameAttack[random] << descriptionAttack[random] << std::endl;
		this->_energy_points -= 25;
		return;
	}
	return;
}

FragTrap & FragTrap::operator=(const FragTrap & rhs) {
	if (this == &rhs)
		return (*this);
	ClapTrap::operator=(rhs);
	return (*this);
}