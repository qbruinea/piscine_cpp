/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/26 16:30:49 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/26 16:31:08 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "ClapTrap.hpp"

ClapTrap::ClapTrap(const ClapTrap &copy) {
	*this = copy;

	return;
}

ClapTrap::ClapTrap(int hp, int max_hp, int ep, int max_ep, int lvl, int melee_dmg, int range_dmg, int armor, std::string name) :
_hit_point(hp), _max_hits_points(max_hp), _energy_points(ep), _max_energy_points(max_ep), _level(lvl), _melee_attack_damage(melee_dmg), _ranged_attack_damage(range_dmg), _armor_damage_reduction(armor), _name(name) {
	std::cout << "A brand new ClapTrap has been created !" << std::endl;

	return;
}

ClapTrap::~ClapTrap() {
	std::cout << "ClapTrap destructor called." << std::endl;

	return;
}

ClapTrap & ClapTrap::operator=(const ClapTrap &rhs) {
	if (this == &rhs)
		return (*this);
	this->_hit_point = rhs._hit_point;
	this->_max_hits_points = rhs._max_hits_points;
	this->_energy_points = rhs._energy_points;
	this->_max_energy_points = rhs._max_energy_points;
	this->_level = rhs._level;
	this->_name = rhs._name;
	this->_melee_attack_damage = rhs._melee_attack_damage;
	this->_ranged_attack_damage = rhs._ranged_attack_damage;
	this->_armor_damage_reduction = rhs._armor_damage_reduction;
	return (*this);
}

void ClapTrap::rangedAttack(const std::string &target) {
	if (this->_hit_point == 0)
		std::cout << this->_name << " is dead. He can't make a ranged attack." << std::endl;
	else
		std::cout << this->_name << " attacks " << target << " at range, causing " << this->_ranged_attack_damage << " points of damage !" << std::endl;
		return;
}

void ClapTrap::meleeAttack(const std::string &target) {
	if (this->_hit_point == 0)
		std::cout << this->_name << " is dead. he can't make a melee attack." << std::endl;
	else
		std::cout << this->_name << " attacks " << target << " at melee, causing " << this->_melee_attack_damage << " points of damage!" << std::endl;
}

void ClapTrap::takeDamage(unsigned int amount) {
	unsigned int	real_dmg = this->_max_hits_points + this->_armor_damage_reduction;

	if ((int)amount < 0)
	{
		std::cout << "You want inflict negativ damage ? Really ?!" << std::endl;
		return;
	}
	if (this->_hit_point == 0)
	{
		std::cout << "You can't attack a dead robot. Is HP is already at " << this->_hit_point << std::endl;
		return;
	}
	if (amount >= real_dmg)
	{
		this->_hit_point = 0;
		std::cout << "FR4G-TP take too much damge, IT'S OVER " << real_dmg << " !!!!!!!! FR4G-TP Must be repaired !" << std::endl;
		return;
	}

	if (amount <= (unsigned int)this->_armor_damage_reduction)
		real_dmg = 0;
	else
		real_dmg = amount - this->_armor_damage_reduction;
	this->_hit_point -= real_dmg;
	if (this->_hit_point < 0)
	{
		this->_hit_point = 0;
		std::cout << this->_name << " hp drop down to " << this->_hit_point << ", he must be repaired !" << std::endl;
		return;
	}
	std::cout << this->_name << " has been attacked, he take " << amount << " but take " << real_dmg << " thank to his armor." << std::endl;

}

void ClapTrap::beRepaired(unsigned int amount) {
	if ((int)amount < 0)
	{
		std::cout << "How could repair with a negativ value ???" << std::endl;
		return;
	}
	if (this->_hit_point == this->_max_hits_points)
	{
		std::cout << "Why would you repair a brand new shiny robot like me ?" << std::endl;
		return;
	}
	if (this->_hit_point + (int)amount >= this->_max_hits_points)
	{
		this->_hit_point = this->_max_hits_points;
		std::cout << this->_name << " is fully restored, well done !" << std::endl;
		return;
	}
	this->_hit_point += amount;
	std::cout << this->_name << " gain " << amount << " hp. He's now at " << this->_hit_point << std::endl;
}

const std::string &ClapTrap::getName() const {
	return (this->_name);
}