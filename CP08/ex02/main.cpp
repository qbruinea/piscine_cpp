/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/21 14:05:43 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/21 14:05:44 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "MutantStack.hpp"

int main()
{
	std::cout << "MUTANTSTACK" << std::endl;

	MutantStack<int> mstack;

	mstack.push(5);
	mstack.push(17);

	std::cout << "MS TOP : " << mstack.top() << std::endl;
	mstack.pop();
	std::cout << "MS SIZE : " << mstack.size() << std::endl;

	mstack.push(3);
	mstack.push(5);
	mstack.push(737);
	mstack.push(0);

	MutantStack<int>::iterator it = mstack.begin();
	MutantStack<int>::iterator ite = mstack.end();
	++it;
	--it;
	while (it != ite)
	{
		std::cout << *it << std::endl;
		++it;
	}
	std::stack<int> s(mstack);

	std::cout << "\nLIST" << std::endl;

	std::list<int> same;
	same.push_back(5);
	same.push_back(17);

	std::cout << "LIST TOP : " << same.back() << std::endl;
	same.pop_back();
	std::cout << "LIST SIZE : " << same.size() << std::endl;

	same.push_back(3);
	same.push_back(5);
	same.push_back(737);
	same.push_back(0);

	std::list<int>::iterator	it2 = same.begin();
	std::list<int>::iterator	ite2 = same.end();
	++it2;
	--it2;
	while (it2 != ite2)
	{
		std::cout << *it2 << std::endl;
		++it2;
	}
	std::list<int>	s2(same);

	return 0;
}

