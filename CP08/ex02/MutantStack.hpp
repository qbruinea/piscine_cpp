/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MutantStack.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/21 14:05:47 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/21 14:05:48 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef MUTANTSTACK_HPP
#define MUTANTSTACK_HPP

#include <iostream>
#include <cstring>
#include <stack>
#include <deque>
#include <list>
#include <vector>

template <class T, class Container = std::deque<T> >
class MutantStack : public std::stack<T, Container> {

public:

	typedef typename Container::iterator iterator;
	typedef typename Container::const_iterator	const_iterator;

	iterator begin() {
		return this->c.begin();
	}
	iterator end() {
		return this->c.end();
	}

};


#endif
