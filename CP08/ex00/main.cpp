/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/21 14:05:33 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/21 14:05:34 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "easyfind.hpp"

int main()
{
	std::vector<int>	vec;
	std::list<int>		list;

	vec.push_back(0);
	vec.push_back(1);
	vec.push_back(2);
	vec.push_back(3);
	vec.push_back(4);
	vec.push_back(5);
	vec.push_back(1000);

	list.push_back(0);
	list.push_back(1);
	list.push_back(2);
	list.push_back(3);
	list.push_back(4);
	list.push_back(5);
	list.push_back(1000);

	try {
		std::vector<int>::iterator it = easyfind(vec, 1000);
		std::cout << "Search 1000 : " << *it << std::endl;
		std::cout << "Previous : " << *(it - 1) << std::endl;

		std::cout << "wrong search : ";
		it = easyfind(vec, 666);
		std::cout << *it << std::endl;
	}
	catch (std::exception & e) {
		std::cout << e.what() << std::endl;
	}

	try {
		std::list<int>::iterator itl = easyfind(list, 4);
		std::cout << "Search 4 : " << *itl << std::endl;
		std::cout << "Previous : " << *(--itl) << std::endl;
	}
	catch (std::exception & e) {
		std::cout << e.what() << std::endl;
	}

	return (0);
}

