/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   easyfind.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/21 14:05:34 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/21 14:05:35 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef EASYFIND_HPP
#define EASYFIND_HPP

#include <iostream>
#include <algorithm>
#include <vector>
#include <list>

class	NotFind : public std::exception {
	virtual const char	*	what() const throw() {
		return ("Not found.");
	}
};

template <typename T>
typename T::iterator	easyfind(T &array, int n) {
	typename T::iterator	it;

	if ((it = std::find(array.begin(), array.end(), n)) != array.end())
		return (it);
	else
		throw NotFind();
}

#endif
