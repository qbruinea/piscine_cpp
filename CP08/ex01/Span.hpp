/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Span.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/21 14:05:41 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/21 14:05:42 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef SPAN_HPP
#define SPAN_HPP

#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>

class Span {

public:

	Span(unsigned int const & n);
	Span(const Span & copy);
	virtual ~Span();
	Span & operator=(const Span & rhs);

	void	addNumber(int n);
	long	shortestSpan();
	long 	longestSpan();

	template<class Iterator>
	void	addNumber(Iterator begin, Iterator end) {
		if (end - begin <= _n)
			std::copy(begin, end, std::back_inserter(this->_tab));
		else
			throw SpanNoSpace();
	}

class SpanFull : public std::exception {
	virtual const char * what() const throw() {
		return ("Span is full.");
	}
};

class SpanNoSpace : public std::exception {
	virtual const char * what() const throw() {
		return ("Not enough space in Span.");
	}
};

class SpanFail : public std::exception {
	virtual const char * what() const throw() {
		return ("Span can't be created.");
	}
};

private:

	Span();

	unsigned int		_n;
	std::vector<int>	_tab;

};

#endif
