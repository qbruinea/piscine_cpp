/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/21 14:05:38 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/21 14:05:39 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Span.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	srand(time(NULL));

	std::cout << "FULL SPAN" << std::endl;
	Span	full(2);

	try {
		full.addNumber(1);
		full.addNumber(2);
		full.addNumber(3);
	}
	catch (const std::exception & e) {
		std::cout << e.what() << std::endl;
	}


	std::cout << "\nEMPTY SPAN" << std::endl;
	Span	empty(10);

	try {
		std::cout << empty.shortestSpan() << std::endl;
	}
	catch (std::exception & e) {
		std::cout << e.what() << std::endl;
	}
	try {
		empty.addNumber(666);
		std::cout << empty.longestSpan() << std::endl;
	}
	catch (std::exception & e) {
		std::cout << e.what() << std::endl;
	}

	std::cout << "\nBIG SPAN" << std::endl;
	Span	big = Span(50000);
	std::vector<int>	range(50000, 10);
	range[666] = 666;
	range[4] = 4;

	big.addNumber(range.begin(), range.end());
	try {
		std::cout << "BIG LONGEST : " << big.longestSpan() << std::endl;
	}
	catch (std::exception & e) {
		std::cout << e.what() << std::endl;
	}
	try {
		std::cout << "BIG SHORTEST : " << big.shortestSpan() << std::endl;
	}
	catch (std::exception & e) {
		std::cout << e.what() << std::endl;
	}

	std::cout << "\nRANDOM SPAN" << std::endl;
	Span	random = Span(2000);

	try {
		for (int i = 0; i < 2000; i++)
			random.addNumber(rand() % 100);
		std::cout << "RANDOM LONGEST : " << random.longestSpan() << std::endl;
	}
	catch (std::exception & e) {
		std::cout << e.what() << std::endl;
	}
	try {
		std::cout << "RANDOM SHORTEST : " << random.shortestSpan() << std::endl;
	}
	catch (std::exception & e) {
		std::cout << e.what() << std::endl;
	}

	std::cout << "\nSUBJECT SPAN" << std::endl;
	Span sp = Span(5);

	try {
		sp.addNumber(5);
		sp.addNumber(3);
		sp.addNumber(17);
		sp.addNumber(9);
		sp.addNumber(11);
		std::cout << "SUBJECT SHORTEST : " << sp.shortestSpan() << std::endl;
	}
	catch (std::exception & e) {
		std::cout << e.what() << std::endl;
	}
	try {
		std::cout << "SUBJECT LONGEST : " << sp.longestSpan() << std::endl;
	}
	catch (std::exception & e) {
		std::cout << e.what() << std::endl;
	}


}
