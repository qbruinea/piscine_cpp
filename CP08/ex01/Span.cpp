/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Span.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/21 14:05:39 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/21 14:05:40 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Span.hpp"

Span::Span(const unsigned int &n) : _n(n) {
	return;
}

Span::Span(const Span &copy) : _n(copy._n), _tab(copy._tab) {
	return;
}

Span::~Span() {
	return;
}

Span & Span::operator=(const Span &rhs) {
	if (this == &rhs)
		return (*this);
	this->_n = rhs._n;
	this->_tab = rhs._tab;
	return (*this);
}

void Span::addNumber(int n) {
	if (this->_tab.size() < this->_n)
		this->_tab.push_back(n);
	else
		throw Span::SpanFull();
}

long Span::shortestSpan() {
	size_t	tab_size = this->_tab.size();
	if (tab_size <= 1)
		throw Span::SpanFail();

	long	shortest = this->_tab[1] - this->_tab[0];

	for (size_t i = 0; i < tab_size - 1; i++)
	{
		if (this->_tab[i + 1] > this-> _tab[i] && this->_tab[i + 1] - this->_tab[i] < shortest)
			shortest = this->_tab[i + 1] - this->_tab[i];
		else if (this->_tab[i] > this->_tab[i + 1] && this->_tab[i] - this->_tab[i + 1] < shortest)
			shortest = this->_tab[i] - this->_tab[i + 1];
	}
	if (shortest < 0)
		return (shortest * -1);
	return (shortest);
}

long Span::longestSpan() {
	size_t	tab_size = this->_tab.size();
	if (tab_size <= 1)
		throw Span::SpanFail();

	std::vector<int>::iterator	min;
	std::vector<int>::iterator	max;

	min = std::min_element(this->_tab.begin(), this->_tab.end());
	max = std::max_element(this->_tab.begin(), this->_tab.end());
	long	ret = (long)*min - (long)*max;

	if (ret < 0)
		return (ret * -1);
	return (ret);
}