/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/16 13:40:12 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/16 13:40:13 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>


class Base {
public:
	virtual ~Base() {}
};

class A : public Base {};
class B : public Base {};
class C : public Base {};

Base * generate()
{
	int random = rand() % 3;

	if (random == 0)
		return (new A);
	else if (random == 1)
		return (new B);
	else if (random == 2)
		return (new C);
	return (NULL);
}

void	identify_from_pointer(Base * p)
{
	if (dynamic_cast<A *>(p))
		std::cout << "Pointer Type : A" << std::endl;
	else if (dynamic_cast<B *>(p))
		std::cout << "Pointer Type : B" << std::endl;
	else if (dynamic_cast<C *>(p))
		std::cout << "Pointer Type : C" << std::endl;
}

void	identify_from_reference(Base & p)
{
	 try
	 {
	 	A &a = dynamic_cast<A &>(p);
		 (void)a;
		 std::cout << "Reference Type : A" << std::endl;
	 }
	 catch (std::bad_cast &e) {}
	 try
	 {
		B &b = dynamic_cast<B &>(p);
		(void)b;
		std::cout << "Reference Type : B" << std::endl;
	 }
	 catch (std::bad_cast &e) {}
	 try
	 {
	 	C &c = dynamic_cast<C &>(p);
	 	(void)c;
	 	std::cout << "Reference Type : C" << std::endl;
	 }
	 catch (std::bad_cast &e) {}
}

int	main()
{
	srand(time(NULL));

	Base	*a = generate();
	identify_from_pointer(a);
	identify_from_reference(*a);

	delete (a);

	return (0);
}

