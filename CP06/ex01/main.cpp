/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/16 13:40:09 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/16 13:40:10 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Data.hpp"

int main()
{
	srand(time(NULL));

	Data	data;

	std::cout << "Simple Data creation : " << data.getS1() << std::endl;
	std::cout << "Simple Data creation : " << data.getRandom() << std::endl;
	std::cout << "Simple Data creation : " << data.getS2() << std::endl;

	Data *test;
	void *ret;

	ret = data.serialize();
	test = data.deserialize(ret);

	std::cout << "Address of ret (serialized data) : " << ret << std::endl;
	std::cout << "Deserialized data : " << test->getS1() << std::endl;
	std::cout << "Deserialized data : " << test->getRandom() << std::endl;
	std::cout << "Deserialized data : " << test->getS2() << std::endl;

	delete test;

	return (0);
}