/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Data.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/16 13:40:06 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/16 13:40:07 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Data.hpp"

Data::Data() {
	_s1 = "       ";
	_s2 = "       ";
	for (int i = 0; i < 7; i++)
	{
		_s1[i] = rand() % 26 + 97;
		_s2[i] = rand() % 26 + 97;
	}
	_s1[7] = 0;
	_s2[7] = 0;
	_random = rand() % INT_MAX;

}

Data::Data(const Data &copy) : _s1(copy.getS1()), _s2(copy.getS2()), _random(copy.getRandom()) {
	return;
}

Data::~Data() {
	return;
}

Data & Data::operator=(const Data &rhs) {
	if (this == &rhs)
		return (*this);
	this->_s1 = rhs.getS1();
	this->_s2 = rhs.getS2();
	this->_random = rhs.getRandom();
	return (*this);
}

void * Data::serialize() {
	std::stringstream	tmp;
	std::string			*tmp1 = new std::string;
	void				*ret;

	tmp << this->_s1 << this->_random << this->_s2;
	*tmp1 = tmp.str();
	ret = tmp1;
	return (ret);
}

Data * Data::deserialize(void *raw) {
	std::string *str;
	std::string	tmp;
	Data 		*ret = new Data();

	str = reinterpret_cast<std::string *>(raw);
	ret->_s1 = str->substr(0, 7);
	tmp = str->substr(7, str->length());

	ret->_s2 = str->substr(str->length() - 7, 7);
	tmp = str->substr(7, str->length() - 7);
	ret->_random = std::atoi(tmp.c_str());

	delete str;
	return (ret);
}

std::string Data::getS1() const {
	return (this->_s1);
}

std::string Data::getS2() const {
	return (this->_s2);
}

int Data::getRandom() const {
	return (this->_random);
}