/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Data.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/16 13:40:08 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/16 13:40:09 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef DATA_HPP
#define DATA_HPP

#include <string>
#include <limits>
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

class Data {

public:

	Data();
	Data(const Data & copy);
	~Data();
	Data & operator=(const Data & rhs);

	void * serialize(void);
	Data * deserialize(void * raw);

	std::string	getS1() const;
	std::string getS2()	const;
	int			getRandom()	const;

private:

	std::string	_s1;
	std::string _s2;
	int 		_random;

};


#endif
