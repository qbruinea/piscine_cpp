/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Squad.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:31:19 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:31:21 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Squad.hpp"

Squad::Squad() : _nb_units(0), _lst_unit(NULL) {
	return;
}

Squad::Squad(const Squad &copy) {
	t_unit	*unit;
	t_unit	*next_unit;
	t_unit	*first_unit;
	t_unit	*copy_unit;

	unit = this->_lst_unit;
	next_unit = unit->_next;
	first_unit = unit;
	copy_unit = copy._lst_unit;
	for (int i = 0; i < this->getCount(); i++)
	{
		delete unit->_unit;
		unit = next_unit;
		next_unit = unit->_next;
	}
	unit = first_unit;
	for (int i = 0; i < copy.getCount(); i++)
	{
		ISpaceMarine *icopy = copy_unit->_unit->clone();
		this->push(icopy);
		copy_unit = copy_unit->_next;
	}
	delete copy._lst_unit;

	return;
}

Squad::~Squad() {
	t_unit	*next;

	for (int i = 0; i < this->getCount(); i++)
	{
		next = this->_lst_unit->_next;
		delete this->_lst_unit->_unit;
		delete this->_lst_unit;
		this->_lst_unit = next;
	}

	return;
}

int Squad::getCount() const {
	return (this->_nb_units);
}

ISpaceMarine*	Squad::getUnit(int index) const {
	t_unit			*unit;
	ISpaceMarine	*marine;

	unit = this->_lst_unit;
	if (index >= 0 && getCount() > index)
	{
		while (index--)
			unit = unit->_next;
		if (unit)
			marine = unit->_unit;
		else
			marine = NULL;
		return (marine);
	}
	return (NULL);
}

int	Squad::push(ISpaceMarine *marine) {
	int 	i;
	t_unit *unit;

	unit = this->_lst_unit;
	if (!marine)
		return (-1);
	if (this->_lst_unit == NULL)
	{
		this->_lst_unit = new t_unit;
		this->_lst_unit->_next = NULL;
		this->_lst_unit->_unit = marine;
		this->_nb_units += 1;
		return (this->getCount());
	}
	int	count = getCount();
	for (i = 0; i < count; i++)
	{
		if (this->getUnit(i) == NULL)
			break ;
	}
	if (i >= getCount())
	{
		while (unit->_next)
			unit = unit->_next;
		unit->_next = new t_unit;
		unit->_next->_unit = marine;
		unit->_next->_next = NULL;
		this->_nb_units += 1;
	}
	return (this->_nb_units);
}

Squad & Squad::operator=(const Squad &rhs) {
	if (this == &rhs)
		return (*this);
	this->_lst_unit = rhs._lst_unit;
	this->_nb_units = rhs._nb_units;
	return (*this);
}