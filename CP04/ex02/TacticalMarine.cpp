/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   TacticalMarine.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:31:23 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:31:23 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "TacticalMarine.hpp"

TacticalMarine::TacticalMarine() {
	std::cout << "Tactical Marine ready for battle!" << std::endl;

	return;
}

TacticalMarine::TacticalMarine(const TacticalMarine &) {
	return;
}

TacticalMarine::~TacticalMarine() {
	std::cout << "Aaargh..." << std::endl;

	return;
}

TacticalMarine & TacticalMarine::operator=(const TacticalMarine &rhs) {
	if (this == &rhs)
		return (*this);
	return (*this);
}

ISpaceMarine*	TacticalMarine::clone() const {
	return (new TacticalMarine(*this));
}

void TacticalMarine::battleCry() const {
	std::cout << "For the holy PLOT!" << std::endl;

	return;
}

void TacticalMarine::rangedAttack() const {
	std::cout << "* attacks with a bolter *" << std::endl;

	return;
}

void TacticalMarine::meleeAttack() const {
	std::cout << "* attacks with a chainsword *" << std::endl;

	return;
}
