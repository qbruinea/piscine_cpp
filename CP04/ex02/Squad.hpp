/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Squad.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:31:21 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:31:22 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef SQUAD_HPP
#define SQUAD_HPP

#include "ISquad.hpp"
#include <iostream>

class Squad : public ISquad {

public:

	Squad();
	Squad(Squad const & copy);
	virtual ~Squad();
	Squad &operator=(const Squad & rhs);

	int				getCount() const ;
	ISpaceMarine*	getUnit(int) const ;

	int				push(ISpaceMarine*);

private:

	typedef struct		s_unit
	{
		ISpaceMarine	*_unit;
		struct s_unit	*_next;
	}					t_unit;

	int		_nb_units;
	t_unit *_lst_unit;

};

#endif
