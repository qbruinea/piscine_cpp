/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   TacticalMarine.hpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:31:25 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:31:25 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef TACTICALMARINE_HPP
#define TACTICALMARINE_HPP

#include "ISpaceMarine.hpp"
#include <iostream>
#include <string>
#include <cctype>

class TacticalMarine : public ISpaceMarine {

public:

	TacticalMarine();
	TacticalMarine(const TacticalMarine&);
	virtual ~TacticalMarine();
	TacticalMarine	&operator=(const TacticalMarine & rhs);

	ISpaceMarine*	clone() const;
	void			battleCry() const;
	void			rangedAttack() const;
	void 			meleeAttack() const;

};

#endif
