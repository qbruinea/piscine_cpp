/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AssaultTerminator.cpp                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:31:08 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:31:09 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "AssaultTerminator.hpp"

AssaultTerminator::AssaultTerminator() {
	std::cout << "* teleports from space *" << std::endl;

	return;
}

AssaultTerminator::AssaultTerminator(const AssaultTerminator &) {
	return;
}

AssaultTerminator::~AssaultTerminator() {
	std::cout << "I'll be back..." << std::endl;

	return;
}

AssaultTerminator & AssaultTerminator::operator=(const AssaultTerminator &rhs) {
	if (this == &rhs)
		return (*this);
	return (*this);
}

ISpaceMarine*	AssaultTerminator::clone() const {
	return (new AssaultTerminator(*this));
}

void AssaultTerminator::battleCry() const {
	std::cout << "This code is unclean. PURIFY IT!" << std::endl;

	return;
}

void AssaultTerminator::rangedAttack() const {
	std::cout << "* does nothing *" << std::endl;

	return;
}

void AssaultTerminator::meleeAttack() const {
	std::cout << "* attacks with chainfists *" << std::endl;

	return;
}