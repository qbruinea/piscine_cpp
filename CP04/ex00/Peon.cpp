/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Peon.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:27:18 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:27:19 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Peon.hpp"

Peon::Peon(std::string name) : Victim(name) {
	std::cout << "Zog, zog." << std::endl;

	return;
}

Peon::Peon(const Peon &copy) : Victim(copy) {
	return;
}

Peon::~Peon() {
	std::cout << "Bleuark..." << std::endl;

	return;
}

Peon & Peon::operator=(const Peon &rhs) {
	if (this == &rhs)
		return (*this);
	Victim::operator=(rhs);
	return (*this);
}

void	Peon::getPolymorph() const {
	std::cout << this->_name << " has been turned into a pink pony!" << std::endl;

	return;
}