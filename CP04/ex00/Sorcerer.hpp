/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Sorcerer.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:27:34 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:27:35 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef SORCERER_HPP
# define SORCERER_HPP

#include <iostream>
#include "Victim.hpp"

class Sorcerer {

public:

	Sorcerer(std::string name, std::string title);
	Sorcerer(Sorcerer const & copy);
	~Sorcerer();
	Sorcerer	&operator=(const Sorcerer & rhs);

	void	polymorph(Victim const &victim);
	std::string	getName() const ;
	std::string	getTitle() const ;


private:

	Sorcerer();

	std::string	_name;
	std::string	_title;

};

std::ostream & operator<<(std::ostream & o, Sorcerer const & rhs);

#endif