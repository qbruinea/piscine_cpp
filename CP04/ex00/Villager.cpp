/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Villager.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:27:40 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:27:41 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Villager.hpp"

Villager::Villager(std::string name) : Victim(name) {
	std::cout << "Heyah!" << std::endl;

	return;
}

Villager::Villager(const Villager &copy) : Victim(copy) {
	return;
}

Villager::~Villager() {
	std::cout << "Ouargl..." << std::endl;

	return;
}

Villager & Villager::operator=(const Villager &rhs) {
	if (this == &rhs)
		return (*this);
	Victim::operator=(rhs);
	return (*this);
}

void	Villager::getPolymorph() const {
	std::cout << this->_name << " has been turned into a frog!" << std::endl;

	return;
}