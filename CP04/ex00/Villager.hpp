/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Villager.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:27:42 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:27:43 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef VILLAGER_HPP
#define VILLAGER_HPP

#include "Victim.hpp"

class Villager : virtual public Victim {

public:

	Villager(std::string name);
	Villager(Villager const & copy);
	virtual ~Villager();
	Villager	&operator=(const Villager & rhs);

	virtual void	getPolymorph() const ;

private:

	Villager();

};


#endif
