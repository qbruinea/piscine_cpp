/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Victim.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:27:38 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:27:39 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef VICTIM_HPP
# define VICTIM_HPP

#include <iostream>

class Victim {

public:

	Victim(std::string name);
	Victim(Victim const & copy);
	virtual ~Victim();
	Victim	&operator=(const Victim & rhs);

	std::string getName() const ;

	virtual void	getPolymorph() const ;

protected:

	std::string	_name;

private:

	Victim();



};

std::ostream & operator<<(std::ostream & o, Victim const & rhs);

#endif
