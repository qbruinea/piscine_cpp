/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Sorcerer.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:27:31 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:27:33 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Sorcerer.hpp"

Sorcerer::Sorcerer(std::string name, std::string title) : _name(name), _title(title) {
	std::cout << name << ", " << title << ", is born!" << std::endl;

	return;
}

Sorcerer::Sorcerer(Sorcerer const &copy) : _name(copy._name), _title(copy._name) {
	return;
}

Sorcerer::~Sorcerer() {
	std::cout << this->_name << ", " << this->_title << ", is dead. Consequences will never be the same!" << std::endl;

	return;
}

Sorcerer & Sorcerer::operator=(const Sorcerer &rhs) {
	if (this == &rhs)
		return (*this);
	this->_name = rhs._name;
	this->_title = rhs._title;
	return (*this);
}

void	Sorcerer::polymorph(const Victim &victim) {
	victim.getPolymorph();

	return;
}

std::string	Sorcerer::getName() const {
	return (this->_name);
}

std::string Sorcerer::getTitle() const {
	return (this->_title);
}

std::ostream & operator<<(std::ostream & o, Sorcerer const & rhs) {
	o << "I am " << rhs.getName() << ", " << rhs.getTitle() << ", and I like ponies!" << std::endl;
	return (o);
}