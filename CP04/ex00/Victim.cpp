/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Victim.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:27:36 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:27:37 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Victim.hpp"

Victim::Victim(std::string name) : _name(name) {
	std::cout << "Some random victim called " << name << " jsut appeared!" << std::endl;

	return;
}

Victim::Victim(const Victim &copy) : _name(copy._name) {
	return;
}

Victim::~Victim() {
	std::cout << "Victim " << this->_name << " just died for no apparent reason!" << std::endl;

	return;
}

Victim & Victim::operator=(const Victim &rhs) {
	if (this == &rhs)
		return (*this);
	this->_name = rhs.getName();
	return (*this);
}

std::string Victim::getName() const {
	return (this->_name);
}

void	Victim::getPolymorph() const {
	std::cout << this->_name << " has been turned into a cute little sheep!" << std::endl;

	return;
}

std::ostream & operator<<(std::ostream & o, Victim const & rhs) {
	o << "I'm " << rhs.getName() << " and I like otters!" << std::endl;
	return (o);
}