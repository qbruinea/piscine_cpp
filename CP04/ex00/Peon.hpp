/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Peon.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:27:30 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:27:31 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef PEON_HPP
# define PEON_HPP

#include "Victim.hpp"
#include "Peon.hpp"
#include "Villager.hpp"

class Peon : virtual public Victim {

public:

	Peon(std::string name);
	Peon(Peon const & copy);
	virtual ~Peon();
	Peon	&operator=(const Peon & rhs);

	virtual void	getPolymorph() const ;

private:

	Peon();

};

#endif