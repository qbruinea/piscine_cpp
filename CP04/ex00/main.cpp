/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:27:13 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:27:14 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Sorcerer.hpp"
#include "Victim.hpp"
#include "Peon.hpp"
#include "Villager.hpp"

int main()
{
	Sorcerer	gerard("Gerard", "the Sourcerer");
	Victim		redbaron("Redbaron");
	Peon		thrall("Thrall");
	Villager	franc("Franc");

	std::cout << std::endl;
	std::cout << gerard << redbaron << thrall << franc;

	std::cout << std::endl;
	gerard.polymorph(redbaron);
	gerard.polymorph(thrall);
	gerard.polymorph(franc);
	std::cout << gerard;

	std::cout << std::endl;
	std::cout << redbaron;
	redbaron.getPolymorph();

	std::cout << std::endl;
	std::cout << thrall;
	thrall.getPolymorph();

	std::cout << std::endl;
	std::cout << franc;
	franc.getPolymorph();

	std::cout << std::endl;

	return (0);
}
/*
int main()
{
	Sorcerer robert("Robert", "the Magnificent");
	Victim jim("Jimmy");
	Peon joe("Joe");
	std::cout << robert << jim << joe;
	robert.polymorph(jim);
	robert.polymorph(joe);
	return 0;
}*/