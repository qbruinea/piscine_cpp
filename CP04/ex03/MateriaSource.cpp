/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MateriaSource.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:33:53 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:33:53 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "MateriaSource.hpp"
#include "Ice.hpp"
#include "Cure.hpp"

MateriaSource::MateriaSource() : _size(0) {
	std::memset(this->_learned, 0, 4);

	return;
}

MateriaSource::MateriaSource(const MateriaSource &copy) : _size(copy._size) {
	for (int i = 0; i < 4; i++)
		this->_learned[i] = copy._learned[i];

	return;
}

MateriaSource::~MateriaSource() {
	for (int i = 0; i < this->_size; i++)
	{
		delete this->_learned[i];
		this->_learned[i] = 0;
	}
	return;
}

MateriaSource & MateriaSource::operator=(const MateriaSource &rhs) {
	if (this == &rhs)
		return (*this);
	for (int i = 0; i < this->_size; i++)
	{
		delete this->_learned[i];
		this->_learned[i] = 0;
	}
	this->_size = rhs._size;
	for (int i = 0; i < rhs._size; i++)
		this->_learned[i] = rhs._learned[i]->clone();
	return (*this);
}

void	MateriaSource::learnMateria(AMateria *learn) {
	this->_learned[this->_size++] = learn;

	return;
}

AMateria * MateriaSource::createMateria(const std::string &type) {
	AMateria *tmp = NULL;

	if (!(type.compare("cure")))
		tmp = new Cure();
	if (!(type.compare("ice")))
		tmp = new Ice();
	return (tmp);
}

