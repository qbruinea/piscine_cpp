/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AMateria.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:31:51 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:32:03 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef AMATERIA_HPP
#define AMATERIA_HPP

#include <iostream>

class ICharacter;

class AMateria {

public:

	AMateria(std::string const & type);
	AMateria(AMateria const & copy);
	virtual	~AMateria();
	AMateria	&operator=(const AMateria & rhs);

	std::string	const & getType() const;
	unsigned int 		getXP() const;

	virtual AMateria*	clone() const = 0;
	virtual void use(ICharacter& target);

protected:

	std::string		_type;
	unsigned int	_xp;

private:

	AMateria();

};

#include "ICharacter.hpp"

#endif
