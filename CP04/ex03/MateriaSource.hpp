/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MateriaSource.hpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:34:00 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:34:06 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef MATERIASOURCE_HPP
#define MATERIASOURCE_HPP

#include "IMateriaSource.hpp"

class MateriaSource : public IMateriaSource {

public:

	MateriaSource();
	MateriaSource(MateriaSource const & copy);
	virtual ~MateriaSource();
	MateriaSource	&operator=(const MateriaSource & rhs);

	virtual void		learnMateria(AMateria* learn);
	virtual AMateria*	createMateria(std::string const & type);

private:

	int			_size;
	AMateria*	_learned[4];

};

#endif
