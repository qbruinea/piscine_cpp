/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:32:07 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:32:08 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Character.hpp"

Character::Character(std::string name) : _name(name), _size(0) {
	std::memset(_inventory, 0, 4);

	return;
}

Character::Character(const Character &copy) :
_name(copy._name), _size(copy._size) {
	for (int i = 0; i < 4; i++)
		this->_inventory[i] = copy._inventory[i];

	return;
}

Character::~Character() {
	for (int i = 0; i < this->_size; i++)
	{
		delete	this->_inventory[i];
		this->_inventory[i] = 0;
	}
	return;
}

Character & Character::operator=(const Character &rhs) {
	if (this == &rhs)
		return (*this);
	this->_name = rhs.getName();
	for (int i = 0; i < this->_size; i++)
	{
		delete	this->_inventory[i];
		this->_inventory[i] = 0;
	}
	this->_size = rhs._size;
	for (int i = 0; i < rhs._size; i++)
		this->_inventory[i] = rhs._inventory[i]->clone();
	return (*this);
}

const std::string & Character::getName() const {
	return (this->_name);
}

void	Character::equip(AMateria *m) {
	if (this->_size < 4)
		this->_inventory[this->_size++] = m;
}

void	Character::unequip(int idx) {
	if (idx >= 0 && idx < this->_size)
	{
		this->_inventory[idx] = 0;
		this->_size--;
	}
}

void	Character::use(int idx, ICharacter &target) {
	if (idx >= 0 && idx < this->_size)
		_inventory[idx]->use(target);
}