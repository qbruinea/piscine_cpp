/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AMateria.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:31:46 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:31:47 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "AMateria.hpp"

AMateria::AMateria(const std::string &type) : _type(type), _xp(0) {
	return;
}

AMateria::AMateria(const AMateria &copy) : _type(copy._type), _xp(copy._xp) {
	return;
}

AMateria::~AMateria() {
	return;
}

AMateria & AMateria::operator=(const AMateria &rhs) {
	if (this == &rhs)
		return (*this);
	this->_type = rhs.getType();
	this->_xp = rhs.getXP();
	return (*this);
}

std::string const & AMateria::getType() const {
	return (this->_type);
}

unsigned int AMateria::getXP() const {
	return (this->_xp);
}

void AMateria::use(ICharacter &target) {
	static_cast<void>(target);
	this->_xp += 10;
}

