/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Ice.hpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:32:59 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:33:06 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef ICE_HPP
#define ICE_HPP

#include "AMateria.hpp"

class Ice : public AMateria{

public:

	Ice();
	Ice(Ice const & copy);
	virtual ~Ice();
	Ice	&operator=(const Ice & rhs);

	virtual AMateria*	clone() const;
	virtual void 		use(ICharacter & target);

};


#endif
