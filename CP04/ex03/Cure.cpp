/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cure.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:32:28 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:32:29 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Cure.hpp"

Cure::Cure() : AMateria("cure") {
	return;
}

Cure::Cure(const Cure &copy) : AMateria(copy) {
	return;
}

Cure::~Cure() {
	return;
}

Cure & Cure::operator=(const Cure &rhs) {
	if (this == &rhs)
		return (*this);
	this->_type = rhs.getType();
	this->_xp = rhs.getXP();
	return (*this);
}

AMateria * Cure::clone() const {
	return (new Cure);
}

void	Cure::use(ICharacter &target) {
	AMateria::use(target);
	std::cout << "* heals " << target.getName() << "\'s wounds *" << std::endl;
}
