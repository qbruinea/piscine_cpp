/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Ice.cpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:32:55 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:32:56 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Ice.hpp"

Ice::Ice() : AMateria("ice") {
	return;
}

Ice::Ice(const Ice &copy) : AMateria(copy) {
	return;
}

Ice::~Ice() {
	return;
}

Ice & Ice::operator=(const Ice &rhs) {
	if (this == &rhs)
		return (*this);
	this->_type = rhs.getType();
	this->_xp = rhs.getXP();
	return (*this);
}

AMateria * Ice::clone() const {
	return (new Ice);
}

void	Ice::use(ICharacter &target) {
	AMateria::use(target);
	std::cout << "* shoots an ice bolt at " << target.getName() << " *" << std::endl;
}