/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cure.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:32:34 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:32:49 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef CURE_HPP
#define CURE_HPP

#include "AMateria.hpp"

class Cure : public AMateria {

public:

	Cure();
	Cure(Cure const & copy);
	virtual ~Cure();
	Cure	&operator=(const Cure & rhs);

	virtual AMateria*	clone() const;
	virtual void 		use(ICharacter & target);

};


#endif
