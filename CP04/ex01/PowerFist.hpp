/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PowerFist.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:28:43 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:28:44 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef POWERFIST_HPP
#define POWERFIST_HPP

#include "AWeapon.hpp"

class PowerFist : virtual public AWeapon {

public:

	PowerFist();
	PowerFist(PowerFist const & copy);
	virtual ~PowerFist();
	PowerFist &operator=(const PowerFist & rhs);

	void	attack() const ;

};


#endif
