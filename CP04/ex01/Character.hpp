/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:28:20 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:28:21 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHARACTER_HPP
#define CHARACTER_HPP

#define EQUIP 1
#define NO_EQUIP 0

#include "AWeapon.hpp"
#include "Enemy.hpp"

class Character {

public:

    Character(std::string const & name);
    Character(Character const & copy);
    ~Character();
    Character &operator=(const Character & rhs);

    void		recoverAP();
    void		equip(AWeapon * weapon);
    void		attack(Enemy * enemy);

    std::string	getName() const ;
    int			getAP() const ;
    AWeapon*	getWeapon () const ;
    int 		isEquip() const ;



private:

    Character();

	std::string	_name;
	int			_ap;
	AWeapon*	_weapon;
	int			_is_equip;

};

std::ostream & operator<<(std::ostream & o, Character const & rhs);

#endif
