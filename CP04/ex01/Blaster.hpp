/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Blaster.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:28:16 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:28:17 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef BLASTER_HPP
#define BLASTER_HPP

#include "AWeapon.hpp"

class Blaster : virtual public AWeapon {

public:

	Blaster();
	Blaster(Blaster const & copy);
	virtual ~Blaster();
	Blaster &operator=(const Blaster & rhs);

	void	attack() const ;

};

#endif
