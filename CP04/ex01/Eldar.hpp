/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Eldar.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:28:25 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:28:26 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef ELDAR_HPP
#define ELDAR_HPP

#include "Enemy.hpp"

class Eldar : virtual public Enemy {

public:

	Eldar();
	Eldar(Eldar const & copy);
	virtual ~Eldar();
	Eldar &operator=(const Eldar & rhs);

};

#endif
