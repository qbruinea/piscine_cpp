/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperMutant.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:28:49 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:28:50 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "SuperMutant.hpp"

SuperMutant::SuperMutant() : Enemy(170, "Super Mutant") {
    std::cout << "Gaaah. Me want smash heads!" << std::endl;

    return;
}

SuperMutant::SuperMutant(const SuperMutant &copy) : Enemy(copy) {
    return;
}

SuperMutant::~SuperMutant() {
    std::cout << "Aaargh..." << std::endl;

    return;
}

SuperMutant & SuperMutant::operator=(const SuperMutant &rhs) {
    if (this == &rhs)
        return (*this);
    Enemy::operator=(rhs);
    return (*this);
}
