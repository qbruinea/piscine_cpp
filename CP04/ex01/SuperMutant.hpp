/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperMutant.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:28:52 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:28:53 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef SUPERMUTANT_HPP
#define SUPERMUTANT_HPP

#include "Enemy.hpp"

class SuperMutant : virtual public Enemy {

public:

	SuperMutant();
	SuperMutant(SuperMutant const & copy);
	virtual ~SuperMutant();
	SuperMutant &operator=(const SuperMutant & rhs);

};

#endif
