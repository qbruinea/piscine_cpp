/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AWeapon.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:28:11 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:28:12 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "AWeapon.hpp"

AWeapon::AWeapon(const std::string &name, int apcost, int damage) :
_name(name), _apcost(apcost), _damage(damage) {
	return;
}

AWeapon::AWeapon(const AWeapon &copy) :
_name(copy._name), _apcost(copy._apcost), _damage(copy._damage) {
	return;
}

AWeapon::~AWeapon() {
	return;
}

AWeapon & AWeapon::operator=(const AWeapon &rhs) {
	if (this == &rhs)
		return (*this);
	this->_name = rhs.getName();
	this->_apcost = rhs.getAPCost();
	this->_damage = rhs.getDamage();
	return (*this);
}

std::string	AWeapon::getName() const {
	return (this->_name);
}

int AWeapon::getAPCost() const {
	return (this->_apcost);
}

int AWeapon::getDamage() const {
	return (this->_damage);
}
