/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Enemy.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:28:27 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:28:28 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Enemy.hpp"

Enemy::Enemy(int hp, const std::string &type) :
_hp(hp), _type(type) {
	return;
}

Enemy::Enemy(const Enemy &copy) : _hp(copy._hp), _type(copy._type) {
	return;
}

Enemy::~Enemy() {
	return;
}

Enemy & Enemy::operator=(const Enemy &rhs) {
	if (this == &rhs)
		return (*this);
	this->_type = rhs.getType();
	this->_hp = rhs.getHP();
	return (*this);
}

std::string Enemy::getType() const {
	return (this->_type);
}

int Enemy::getHP() const {
	return (this->_hp);
}

void Enemy::takeDamage(int damage) {
	if (damage < 0)
		return;
	this->_hp -= damage;
}