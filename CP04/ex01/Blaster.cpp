/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Blaster.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:28:14 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:28:15 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Blaster.hpp"

Blaster::Blaster() : AWeapon("Blaster", 3, 12) {
	return;
}

Blaster::Blaster(const Blaster &copy) : AWeapon(copy) {
	return;
}

Blaster::~Blaster() {
	return;
}

Blaster & Blaster::operator=(const Blaster &rhs) {
	if (this == &rhs)
		return (*this);
	AWeapon::operator=(rhs);
	return (*this);
}

void	Blaster::attack() const {
	std::cout << "* blaaaa ratatatata *" << std::endl;

	return;
}
