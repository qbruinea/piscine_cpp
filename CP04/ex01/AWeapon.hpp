/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AWeapon.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:28:12 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:28:13 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef AWEAPON_HPP
#define AWEAPON_HPP

#include <iostream>

class AWeapon {

public:

	AWeapon(std::string const & name, int apcost, int damage);
	AWeapon(AWeapon const & copy);
	virtual ~AWeapon();
	AWeapon	&operator=(const AWeapon & rhs);

	std::string	getName() const ;
	int			getAPCost() const ;
	int 		getDamage() const ;

	virtual void	attack() const = 0;

protected:

	std::string _name;
	int			_apcost;
	int 		_damage;

private:

	AWeapon();

};


#endif
