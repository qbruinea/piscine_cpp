/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Eldar.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:28:23 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:28:24 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Eldar.hpp"

Eldar::Eldar() : Enemy(50, "Eldar") {
	std::cout << "Behold, the fiery destroyer of worlds." << std::endl;

	return;
}

Eldar::Eldar(const Eldar &copy) : Enemy(copy) {
	return;
}

Eldar::~Eldar() {
	std::cout << "For the... argl... x_x" << std::endl;

	return;
}

Eldar & Eldar::operator=(const Eldar &rhs) {
	if (this == &rhs)
		return (*this);
	Enemy::operator=(rhs);
	return (*this);
}