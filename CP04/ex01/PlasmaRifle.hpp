/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PlasmaRifle.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:28:36 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:28:37 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef PLASMARIFLE_HPP
#define PLASMARIFLE_HPP

#include "AWeapon.hpp"

class PlasmaRifle : virtual public AWeapon {

public:

	PlasmaRifle();
	PlasmaRifle(PlasmaRifle const & copy);
	virtual ~PlasmaRifle();
	PlasmaRifle &operator=(const PlasmaRifle & rhs);

	void	attack() const ;

};


#endif
