/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RadScorpion.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:28:47 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:28:48 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef RADSCORPION_HPP
#define RADSCORPION_HPP

#include "Enemy.hpp"

class RadScorpion : virtual public Enemy {

public:

    RadScorpion();
    RadScorpion(RadScorpion const & copy);
    virtual ~RadScorpion();
    RadScorpion &operator=(const RadScorpion & rhs);

};

#endif
