/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:28:18 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:28:20 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Character.hpp"

Character::Character(const std::string &name) : _name(name), _ap(40), _is_equip(NO_EQUIP) {
	return;
}

Character::Character(const Character &copy) : _name(copy._name), _ap(copy._ap), _is_equip(copy._is_equip) {
	if (copy._is_equip)
		this->_weapon = copy._weapon;

	return;
}

Character::~Character() {
	return;
}

Character & Character::operator=(const Character &rhs) {
	if (this == &rhs)
		return (*this);
	this->_name = rhs.getName();
	this->_ap = rhs.getAP();
	this->_is_equip = rhs._is_equip;
	if (rhs.isEquip())
		this->_weapon = rhs._weapon;
	return (*this);
}

void Character::recoverAP() {
	if (this->_ap == 40)
		std::cout << this->_name << " is already full AP." << std::endl;
	else if (this->_ap >= 30)
	{
		this->_ap = 40;
		std::cout << this->_name << " recover all his AP."<< std::endl;
	}
	else
	{
		this->_ap += 10;
		std::cout << this->_name << " take a break to recover some AP."  << this->_ap << "/40" << std::endl;
	}
	return;
}

void Character::equip(AWeapon *weapon) {
	this->_is_equip = EQUIP;
	this->_weapon = weapon;
}

void Character::attack(Enemy *enemy) {
	if (this->_ap < this->_weapon->getAPCost())
		std::cout << "Your AP is low." << std::endl;
	else if (!enemy)
		std::cout << "You attack nothing." << std::endl;
	else if (this->_is_equip)
	{
		std::cout << this->getName() << " attack " << enemy->getType() << " with " << this->_weapon->getName() << std::endl;
		enemy->takeDamage(this->_weapon->getDamage());
		this->_weapon->attack();
		this->_ap -= this->_weapon->getAPCost();
		if (enemy->getHP() <= 0)
			delete enemy;
	}
	return;
}

std::string	Character::getName() const {
	return (this->_name);
}

int Character::getAP() const {
	return (this->_ap);
}

AWeapon* Character::getWeapon() const {
	return (this->_weapon);
}

int Character::isEquip() const {
	return (this->_is_equip);
}

std::ostream & operator<<(std::ostream & o, Character const & rhs) {

	if (rhs.isEquip())
		o << rhs.getName() << " has " << rhs.getAP() << " AP and wields a " << rhs.getWeapon()->getName() << std::endl;
	else
		o << rhs.getName() << " has " << rhs.getAP() << " AP and is unarmed" << std::endl;
	return (o);
}