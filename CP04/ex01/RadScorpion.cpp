/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RadScorpion.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:28:45 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:28:46 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "RadScorpion.hpp"

RadScorpion::RadScorpion() : Enemy(80, "Rad Scorpion") {
    std::cout << "* click click click *" << std::endl;

    return;
}

RadScorpion::RadScorpion(const RadScorpion &copy) : Enemy(copy) {
    return;
}

RadScorpion::~RadScorpion() {
    std::cout << "* SPROTCH *" << std::endl;

    return;
}

RadScorpion & RadScorpion::operator=(const RadScorpion &rhs) {
    if (this == &rhs)
        return (*this);
    Enemy::operator=(rhs);
    return (*this);
}
