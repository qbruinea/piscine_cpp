/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Enemy.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:28:29 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:28:31 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef ENEMY_HPP
#define ENEMY_HPP

#include <iostream>

class Enemy {

public:

	Enemy(int hp, std::string const & type);
	Enemy(Enemy const & copy);
	virtual ~Enemy();
	Enemy &operator=(const Enemy & rhs);

	std::string	getType() const ;
	int 		getHP() const ;

	virtual void takeDamage(int damage);

protected:

	int			_hp;
	std::string	_type;

private:

	Enemy();

};

#endif
