/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PowerFist.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:28:41 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:28:42 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "PowerFist.hpp"

PowerFist::PowerFist() : AWeapon("Power Fist", 8, 50) {
	return;
}

PowerFist::PowerFist(const PowerFist &copy) : AWeapon(copy) {
	return;
}

PowerFist::~PowerFist() {
	return;
}

PowerFist & PowerFist::operator=(const PowerFist &rhs) {
	if (this == &rhs)
		return (*this);
	AWeapon::operator=(rhs);
	return (*this);
}

void	PowerFist::attack() const {
	std::cout << "* pschhh... SBAM! *" << std::endl;

	return;
}