/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:28:32 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/06 18:30:33 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "AWeapon.hpp"
#include "PowerFist.hpp"
#include "RadScorpion.hpp"
#include "Enemy.hpp"
#include "SuperMutant.hpp"
#include "Character.hpp"
#include "PlasmaRifle.hpp"
#include "Eldar.hpp"
#include "Blaster.hpp"

int	main()
{
	Character* me = new Character("Karl-Franz");

	std::cout << std::endl << *me << std::endl;

	Enemy* b = new RadScorpion();
	Enemy* c = new Eldar();
	Enemy* d = new SuperMutant();

	AWeapon* blaster = new Blaster();
	AWeapon* pr = new PlasmaRifle();
	AWeapon* pf = new PowerFist();

	me->equip(pr);
	std::cout << *me << std::endl;
	me->equip(pf);
	me->attack(b);
	std::cout << *me;
	me->equip(pr);
	std::cout << *me;
	me->attack(b);
	std::cout << *me;
	me->attack(b);
	std::cout << *me;
	me->equip(blaster);
	me->attack(c);
	std::cout << *me;
	me->equip(pf);
	me->attack(c);
	std::cout << *me;
	me->equip(blaster);
	me->attack(d);
	me->attack(d);
	me->attack(d);
	me->attack(d);
	me->recoverAP();
	me->recoverAP();
	me->recoverAP();
	me->recoverAP();
	me->recoverAP();
	me->equip(pf);
	me->attack(d);
	me->attack(d);
	me->attack(d);

	delete me;
	delete pr;
	delete pf;
	delete blaster;

	return (0);
}
