/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/18 16:23:56 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/18 16:24:52 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"
#include "ZombieEvent.hpp"

int main(void)
{
	ZombieEvent	zombox_event;
	ZombieEvent	zombox_event2("CPT");
	Zombie		Heap_zombie;
	Zombie*		zombox;

	Heap_zombie.setName("Gerard");
	Heap_zombie.setType("Depute");
	Heap_zombie.announce();

	zombox = new Zombie("Fast", "Ussain");
	zombox->announce();
	delete zombox;

	zombox = zombox_event2.newZombie("Nani");
	zombox->announce();
	delete zombox;

	zombox_event.setZombieType("Vieux");
	zombox = zombox_event.randomChump();
	delete zombox;

	zombox_event.setZombieType("Ninja");
	zombox = zombox_event.newZombie("DANIEL");
	zombox->announce();
	delete zombox;

	return (0);
}