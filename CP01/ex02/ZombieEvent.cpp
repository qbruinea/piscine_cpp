/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/18 16:24:05 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/18 16:24:52 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieEvent.hpp"

ZombieEvent::ZombieEvent(void) {
	return;
}

ZombieEvent::ZombieEvent(std::string type) : _type(type) {
	return;
}

ZombieEvent::~ZombieEvent(void) {
	return;
}

void	ZombieEvent::setZombieType(const std::string &type) {
	this->_type = type;
	return;
}

const std::string &ZombieEvent::getZombieType() const {
	return (this->_type);
}

Zombie*	ZombieEvent::newZombie(std::string name) {
	return (new Zombie(this->_type, name));
}

Zombie*	ZombieEvent::randomChump() {
	int		random;
	Zombie*	random_zonzon;
	std::string	name[8] = {
		"Roger",
		"Andre",
		"Michel",
		"Didier",
		"Ginnette",
		"Gislaine",
		"Michelle",
		"Mireille"
	};

	srand(time(NULL));
	random = rand() % 8;
	if (this->_type == "")
		this->_type = "CPT2";
	random_zonzon = newZombie(name[random]);
	random_zonzon->announce();

	return (random_zonzon);
}