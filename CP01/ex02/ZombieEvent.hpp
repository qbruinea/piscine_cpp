/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/18 16:24:07 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/18 16:24:52 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIEEVENT_HPP
# define ZOMBIEEVENT_HPP

#include <cstdlib>
#include <ctime>
#include <iostream>
#include "Zombie.hpp"

class	ZombieEvent {

public:

	ZombieEvent(void);
	ZombieEvent(std::string type);
	~ZombieEvent(void);

	void	setZombieType(const std::string &type);
	const	std::string	&getZombieType() const ;

	Zombie*	newZombie(std::string name);
	Zombie*	randomChump();

private:

	std::string _type;

};

#endif