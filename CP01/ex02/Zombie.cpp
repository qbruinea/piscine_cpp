/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/18 16:24:00 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/18 16:24:52 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"

Zombie::Zombie(void) {
	return;
}

Zombie::Zombie(std::string type, std::string name) : _name(name), _type(type) {
	return;
}

Zombie::~Zombie(void) {
	std::cout << this->_name << " is dead." << std::endl;
	return;
}

void	Zombie::setName(const std::string &name) {
	this->_name = name;
	return;
}

void	Zombie::setType(const std::string &type) {
	this->_type = type;
	return;
}

const	std::string &Zombie::getName() const {
	return (this->_name);
}

const	std::string &Zombie::getType() const {
	return (this->_type);
}

void	Zombie::announce() const {
	std::cout << "<" << this->getName() << " (" << this->getType() << ")>  Braiiiiiiinnnssss ..." << std::endl;
}