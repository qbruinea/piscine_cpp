/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieHorde.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/18 16:24:17 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/18 16:24:52 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieHorde.hpp"

ZombieHorde::ZombieHorde(int n) {
    int random;

    if (n <= 0 || n >= 2147483647)
    {
        std::cout << "Invalid numbere of zombies. Well, say that there is a horde of 6." << std::endl;
        n = 6;
    }
    this->_zombie = new Zombie[n];
    this->_nb_zombie = n;
    srand(time(NULL));

    	std::string	name[12] = {
		"Roger",
		"Andre",
		"Michel",
		"Didier",
		"Ginnette",
		"Gislaine",
		"Michelle",
		"Mireille",
        "Bernadette",
        "Jean-Eude",
        "Mikeline",
        "Robert"
	};

    for (int i = 0; i < n; i++)
    {
        random = rand() % 12;
        this->_zombie[i].setType("slow");
        this->_zombie[i].setName(name[random]);
    }

    return;
}

ZombieHorde::~ZombieHorde() {
    delete [] _zombie;

    return;
}

void    ZombieHorde::announce() {
    for (int i = 0; i < _nb_zombie; i++)
    {
        _zombie[i].announce();
    }

    return;
}