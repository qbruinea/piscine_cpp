/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pony.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/18 16:23:45 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/18 16:24:52 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Pony.hpp"

Pony::Pony(void) {
	return;
}

Pony::~Pony(void) {
	return;
}

void	Pony::setPrenom(const std::string &prenom) {
	this->_prenom = prenom;
	return;
}

void	Pony::setRace(const std::string &race) {
	this->_race = race;
	return;
}

void	Pony::setPropietaire(const std::string &proprietaire) {
	this->_proprietaire = proprietaire;
	return;
}

void	Pony::setCompetence(const std::string &competence) {
	this->_competence = competence;
	return;
}

const	std::string	&Pony::getPrenom() const {
	return (this->_prenom);
}

const	std::string &Pony::getRace() const {
	return (this->_race);
}

const	std::string &Pony::getProprietaire() const {
	return (this->_proprietaire);
}

const	std::string &Pony::getCompetence() const {
	return (this->_competence);
}
