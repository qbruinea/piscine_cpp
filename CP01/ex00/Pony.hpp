/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pony.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/18 16:23:48 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/18 16:24:52 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef PONY_HPP
# define PONY_HPP

#include <iostream>

class Pony {

public:

	Pony(void);
	~Pony(void);

	void	setPrenom(const std::string &prenom);
	void	setRace(const std::string &race);
	void	setPropietaire(const std::string &proprietaire);
	void	setCompetence(const std::string &competence);

	const std::string	&getPrenom() const ;
	const std::string	&getRace() const ;
	const std::string	&getProprietaire() const ;
	const std::string	&getCompetence() const ;

private:

	std::string _prenom;
	std::string _race;
	std::string _proprietaire;
	std::string _competence;

};

#endif