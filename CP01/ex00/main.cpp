/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/18 16:23:39 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/18 16:24:52 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Pony.hpp"

void	ponyOnTheStack(Pony stack) {
	std::string	entry;

	std::cout << "Name of the pony stack : ";
	std::getline(std::cin, entry);
	stack.setPrenom(entry);

	std::cout << "Race of the pony stack : ";
	std::getline(std::cin, entry);
	stack.setRace(entry);

	std::cout << "Owner of the pony stack : ";
	std::getline(std::cin, entry);
	stack.setPropietaire(entry);

	std::cout << "Skill of the pony stack : ";
	std::getline(std::cin, entry);
	stack.setCompetence(entry);

	std::cout << "The name of the pony on the stack is : " << stack.getPrenom();
	std::cout << ". He is the race of : " << stack.getRace();
	std::cout << ". " << stack.getProprietaire() << " is his owner.";
	std::cout << " And the pony special skill is : " << stack.getCompetence() << std::endl;
}

void	ponyOnTheHeap(Pony* heap)
{
	std::string	entry;

	std::cout << "Name of the pony heap : ";
	std::getline(std::cin, entry);
	heap->setPrenom(entry);

	std::cout << "Race of the pony heap : ";
	std::getline(std::cin, entry);
	heap->setRace(entry);

	std::cout << "Owner of the pony heap : ";
	std::getline(std::cin, entry);
	heap->setPropietaire(entry);

	std::cout << "Skill of the pony heap : ";
	std::getline(std::cin, entry);
	heap->setCompetence(entry);

	std::cout << "The name of the pony on the heap is : " << heap->getPrenom();
	std::cout << ". He is the race of : " << heap->getRace();
	std::cout << ". " << heap->getProprietaire() << " is his owner.";
	std::cout << " And the pony special skill is : " << heap->getCompetence() << std::endl;

	delete heap;
}

int main(void)
{
	Pony	stack;
	Pony*	heap = new Pony;

	ponyOnTheStack(stack);
	ponyOnTheHeap(heap);
	while (1);
	return (0);
}