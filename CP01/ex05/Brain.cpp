/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Brain.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/18 16:24:27 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/18 16:24:52 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Brain.hpp"

Brain::Brain() {
	return;
}

Brain::~Brain() {
	return;
}

std::string	Brain::identify() const {
	std::string	id_brain = "You're brain is here : ";
	const void* address = static_cast<const void*>(this);
	std::stringstream ss;
	ss << address;
	std::string add = ss.str();
	id_brain.append(add);
	return (id_brain);
}