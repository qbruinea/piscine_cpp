/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Replace.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/18 17:14:31 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/19 20:56:18 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <stdio.h>

static void _replace(const char *fd, const char *s1, const char *s2)
{
	std::ostringstream	oss;
	std::string 		str_search (s1);
	std::string 		str_replace (s2);
	std::ifstream		ifs (fd);
	std::string			filename (fd);
	std::string 		str;
    std::string         is_eof;
	size_t				pos;
    size_t              i = 0;

	oss << ifs.rdbuf();
	str = oss.str();
    std::cout << str << std::endl;
    ifs.close();
    ifs.open(filename);

    while(std::getline(ifs, is_eof))
    {
        i++;
    }
    std::cout << i << std::endl;
    
	while (i > 0)
	{
        i--;
        std::cout << str << std::endl;
		pos = str.find(str_search);
		if (pos == std::string::npos)
			break;
		str.replace(pos, str_search.length(), str_replace);
        std::cout << i << std::endl;
        
	}
	ifs.close();
	std::ofstream		ofs(filename + ".replace");
	ofs << str;
	std::cout << filename << ".replace has been created." << std::endl;
}

static void	_entry_error(int ac, char **av)
{
	if (ac != 4)
	{
		std::cout << "Wrong number of args, make sure you specified a file, the string to find and the remplacement string (string mustn't be empty" << std::endl;
		exit (0);
	}
	if (av[2] == "" || av[3] == "")
	{
		std::cout << "Occurrence or remplacement string must not be empty" << std::endl;
		exit (0);
	}
    if (av[2] == av[3])
    {
        std::cout << "search and replace are the same." << std::endl;
        exit (0);
    }
	std::ifstream ifs (av[1]);
	if (ifs.is_open())
		return;
	else
	{
		std::cout << "File does not exist !" << std::endl;
		exit (0);
	}
}

int 		main(int ac, char **av)
{
	_entry_error(ac, av);
	_replace(av[1], av[2], av[3]);
	return (0);
}