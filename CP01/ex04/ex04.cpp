/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex04.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/18 16:24:22 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/18 16:24:52 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

int main(void)
{
	std::string		str = "HI THIS IS BRAIN";
	std::string&	strRef = str;
	std::string*	strPtr = &str;

	std::cout << "str = " << str << std::endl;
	std::cout << "strRef = " << strRef << std::endl;
	std::cout << "strPtr = " << strPtr << std::endl;
	strRef = "kek";
	std::cout << "str = " << str << std::endl;
	std::cout << "strRef = " << strRef << std::endl;
	std::cout << "strPtr = " << strPtr << std::endl;

	return (0);
}