/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/22 17:46:42 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/22 17:47:06 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef FIXED_HPP
# define FIXED_HPP

#include <iostream>

class Fixed {

public:

	Fixed();
	Fixed(Fixed const & copy);
	~Fixed();

	Fixed	&operator=(const Fixed& rhs);

	int		getRawBits(void) const ;
	void	setRawBits(int const raw);

private:

	int					_value;
	const static int	_fractional_bits;

};

#endif