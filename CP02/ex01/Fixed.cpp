/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/22 17:46:52 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/22 17:47:06 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.hpp"

Fixed::Fixed() {
	std::cout << "Default constructor called." << std::endl;
	this->_value = 0;
	return;
}

Fixed::Fixed(const int integer) {
	std::cout << "Integer constructor called." << std::endl;
	this->_value = integer << this->_fractional_bits;
}

Fixed::Fixed(const float floating) {
	std::cout << "Float constructor called." << std::endl;
	this->_value = (int)(roundf(floating * (1 << this->_fractional_bits)));
}

Fixed::Fixed(const Fixed &copy) {
	std::cout << "Copy constructor called." << std::endl;
	this->_value = copy.getRawBits();
	return;
}

Fixed::~Fixed() {
	std::cout << "Destructor called" << std::endl;
	return;
}

Fixed & Fixed::operator=(const Fixed & rhs) {
	std::cout << "Assignation operator called." << std::endl;
	if (this == &rhs)
		return (*this);
	this->_value = rhs.getRawBits();
	return (*this);
}

int Fixed::getRawBits() const {
	std::cout << "getRawBits member function called." << std::endl;
	return (this->_value);
}

void Fixed::setRawBits(const int raw) {
	this->_value = raw;
	return;
}

float Fixed::toFloat() const {
	return ((float)this->_value / (float)(1 << this->_fractional_bits));
}

int	Fixed::toInt() const {
	return ((int)this->_value >> this->_fractional_bits);
}

int const Fixed::_fractional_bits = 8;

std::ostream & operator<<(std::ostream & o, Fixed const & rhs) {
	o << rhs.toFloat();
	return (o);
}