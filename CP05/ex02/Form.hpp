/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Form.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/10 17:19:51 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/10 17:19:52 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef FORM_HPP
#define FORM_HPP

#include "Bureaucrat.hpp"
#include <fstream>
#include <stdlib.h>
#include <time.h>

class Bureaucrat;

class Form {

public:

	Form(std::string const & name, unsigned int grade, unsigned int exec_grade);
	Form(const Form & copy);
	virtual ~Form();
	Form & operator=(const Form & rhs);

	std::string		getName() const;
	bool 			getSigned() const;
	unsigned int	getSignGrade() const;
	unsigned int	getExecGrade() const;

	void			setSigned();

	void            beSigned(Bureaucrat const & target);

	virtual void    execute(Bureaucrat const & exec) const = 0;

    class GradeTooLowException : public std::exception {
    public:
        virtual const char * what() const throw() {
            return ("Grade too low");
        }
    };

    class GradeTooHighException : public std::exception {
    public:
        virtual const char * what() const throw() {
            return ("Grade too high");
        }
    };

private:

	Form();

	const std::string	_name;
	bool 				_is_signed;
	const unsigned int	_sign_grade;
	const unsigned int	_exec_grade;

};

std::ostream & operator<<(std::ostream & o, Form const & rhs);

#endif
