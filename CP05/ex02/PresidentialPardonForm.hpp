/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PresidentialPardonForm.hpp                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/13 13:31:48 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/13 13:31:49 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef PRESIDENTIALPARDONFORM_HPP
#define PRESIDENTIALPARDONFORM_HPP

#include "Form.hpp"

class PresidentialPardonForm : public Form {

public:

    PresidentialPardonForm(std::string const & target);
    PresidentialPardonForm(const PresidentialPardonForm & copy);
    virtual ~PresidentialPardonForm();
    PresidentialPardonForm & operator=(const PresidentialPardonForm & rhs);

    virtual void   execute(Bureaucrat const & exec) const;

private:

    PresidentialPardonForm();

    const std::string   _target;

};

#endif
