/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RobotomyRequestForm.hpp                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/13 13:31:46 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/13 13:31:47 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef ROBOTOMYREQUESTFORM_HPP
#define ROBOTOMYREQUESTFORM_HPP

#include "Form.hpp"

class RobotomyRequestForm : public Form {

public:

    RobotomyRequestForm(std::string const & target);
    RobotomyRequestForm(const RobotomyRequestForm & copy);
    virtual ~RobotomyRequestForm();
    RobotomyRequestForm & operator=(const RobotomyRequestForm & rhs);

    virtual void  execute(Bureaucrat const & exec) const;

private:

    RobotomyRequestForm();

    std::string const   _target;

};

#endif
