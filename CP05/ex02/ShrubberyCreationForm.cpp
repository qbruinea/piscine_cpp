/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ShrubberyCreationForm.cpp                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/13 13:31:44 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/13 13:31:46 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "ShrubberyCreationForm.hpp"

ShrubberyCreationForm::ShrubberyCreationForm(const std::string &target) :
Form("ShrubberyCreationForm", 145, 137), _target(target) {
    return;
}

ShrubberyCreationForm::ShrubberyCreationForm(const ShrubberyCreationForm &copy) :
Form(copy), _target(copy._target) {
    return;
}

ShrubberyCreationForm::~ShrubberyCreationForm() {
    return;
}

ShrubberyCreationForm & ShrubberyCreationForm::operator=(const ShrubberyCreationForm &rhs) {
    if (this == &rhs)
        return (*this);
    Form::operator=(rhs);
    return (*this);
}

void ShrubberyCreationForm::execute(const Bureaucrat &exec) const {
    if (this->getSigned() == 0)
        std::cout << this->getName() << " can't be execute, it require a signature." << std::endl;
    else if (this->getExecGrade() < exec.getGrade())
        std::cout << exec.getName() << " cannot execute this form, his grade is too low." << std::endl;
    else {
        std::ofstream o;
        o.open(this->_target + "_shrubbery");
        o <<
        ".    .     .  .      +     .      .          .\n"
        ".            .      .    #       .           .\n"
        ".           .           ###            .      .      .\n"
        ".           .   \"#:. .:##\"##:. .:#\"  .      \n"
        ".            .    \"####\"###\"####\"  .\n"
        ".           \"#:.    .:#\"###\"#:.    .:#\"  .        .       .\n"
        ".              \"#########\"#########\"        .        .\n"
        ".           \"#:.  \"####\"###\"####\"  .:#\"   .       .\n"
        ".         .  \"#######\"\"##\"##\"\"#######\"                  .\n"
        ".               \"##\"#####\"#####\"##\"           .      .\n"
        ".      \"#:. ...  .:##\"###\"###\"##:.  ... .:#\"     .\n"
        ".          \"#######\"##\"#####\"##\"#######\"      .     .\n"
        ".     .       \"#####\"\"#######\"\"#####\"    .      .\n"
        ".      \"                000      \"    .     .\n"
        ".          .   .        000     .        .       .\n"
        ".. .. .................O000O........................ ...... ...\n";
    }
    return;
}
