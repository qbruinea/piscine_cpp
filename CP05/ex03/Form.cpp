/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Form.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/10 17:19:48 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/10 17:19:49 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Form.hpp"

Form::Form(const std::string &name, unsigned int grade, unsigned int exec_grade) : _name(name), _is_signed(false), _sign_grade(grade), _exec_grade(exec_grade) {
    if (grade < 1)
        throw Form::GradeTooHighException();
    else if (grade > 150)
        throw Form::GradeTooLowException();
    if (exec_grade < 1)
        throw Form::GradeTooHighException();
    else if (exec_grade > 150)
        throw Form::GradeTooLowException();
    return;
}

Form::Form(const Form &copy) : _name(copy._name), _is_signed(copy._is_signed), _sign_grade(copy._sign_grade), _exec_grade(copy._exec_grade) {
    return;
}

Form::~Form() {
    return;
}

Form & Form::operator=(const Form &rhs) {
    if (this == &rhs)
        return (*this);
    this->_is_signed = rhs._is_signed;
    return (*this);
}

std::string Form::getName() const {
	return (this->_name);
}

bool Form::getSigned() const {
	return (this->_is_signed);
}

unsigned int Form::getSignGrade() const {
	return (this->_sign_grade);
}

unsigned int Form::getExecGrade() const {
	return (this->_exec_grade);
}

void Form::setSigned() {
	this->_is_signed = true;
}

void Form::beSigned(const Bureaucrat &target) {
    if (this->_is_signed)
    {
        std::cout << "Form already signed." << std::endl;
        return;
    }
    if (target.getGrade() > this->_sign_grade)
        throw Form::GradeTooLowException();
    else
	{
    	std::cout << target.getName() << " signed form " << this->_name << std::endl;
    	this->setSigned();
	}
	return;
}

std::ostream & operator<<(std::ostream & o, Form const & rhs)
{
	o << "Form <" << rhs.getName() << "> required a grade <" << rhs.getSignGrade() << "> to bee signed. Signed status is actually : " << rhs.getSigned() << std::endl;
	return (o);
}