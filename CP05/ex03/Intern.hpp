/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Intern.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/13 13:35:41 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/13 13:35:42 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef INTERN_HPP
#define INTERN_HPP

#include "Form.hpp"

class Intern {

public:

	Intern();
	Intern(const Intern & copy);
	~Intern();
	Intern & operator=(const Intern & rhs);

	Form * makeForm(std::string const & form, std::string const & target);

};


#endif
