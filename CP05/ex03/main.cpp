/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/10 17:19:56 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/10 17:19:57 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "ShrubberyCreationForm.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "Intern.hpp"

int main()
{
    srand(time(NULL));
	try
	{
		Bureaucrat	gerard("Gerard", 46);
		Bureaucrat	didier("Didier", 149);
		Bureaucrat	ginette("Ginette", 2);

		Intern		kevin;

		RobotomyRequestForm		form1("Stroheim");
		PresidentialPardonForm	form2("Jean-Castex");
		ShrubberyCreationForm	form3("home");

		Form * internlabber;

		std::cout << gerard << didier << ginette;
		gerard.signForm(form3);
		ginette.signForm(form2);
		ginette.signForm(form1);
		form1.execute(gerard);
		form2.execute(gerard);
		form3.execute(didier);
		gerard.executeForm(form3);
		ginette.executeForm(form2);
		ginette.executeForm(form1);

		internlabber = kevin.makeForm("g", "Le pape");
		ginette.signForm(*internlabber);
		internlabber->execute(ginette);

		delete internlabber;

	}
	catch (const Bureaucrat::GradeTooLowException& e)
	{
		std::cout << e.what() << std::endl;
		return (1);
	}
	catch (const Bureaucrat::GradeTooHighException& e)
	{
		std::cout << e.what() << std::endl;
		return (1);
	}
	catch (const Form::GradeTooLowException& e)
	{
		std::cout << e.what() << std::endl;
		return (1);
	}
	catch (const Form::GradeTooHighException& e)
	{
		std::cout << e.what() << std::endl;
		return (1);
	}

	return (0);
}