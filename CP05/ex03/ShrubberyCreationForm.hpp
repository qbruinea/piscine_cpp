/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ShrubberyCreationForm.hpp                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/13 13:31:40 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/13 13:31:46 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHRUBBERYCREATIONFORM_HPP
#define SHRUBBERYCREATIONFORM_HPP

#include "Form.hpp"

class ShrubberyCreationForm : public Form {

public:

    ShrubberyCreationForm(std::string const & target);
    ShrubberyCreationForm(const ShrubberyCreationForm & copy);
    virtual ~ShrubberyCreationForm();
    ShrubberyCreationForm & operator=(const ShrubberyCreationForm & rhs);

    virtual void    execute(Bureaucrat const & exec) const;

private:

    ShrubberyCreationForm();

    std::string const _target;

};

#endif
