/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RobotomyRequestForm.cpp                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/13 13:31:47 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/13 13:31:48 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "RobotomyRequestForm.hpp"

RobotomyRequestForm::RobotomyRequestForm(const std::string &target) :
Form("RobotomyRequestForm", 72, 45), _target(target) {
	return;
}

RobotomyRequestForm::RobotomyRequestForm(const RobotomyRequestForm &copy) :
Form(copy), _target(copy._target) {
	return;
}

RobotomyRequestForm::~RobotomyRequestForm() {
	return;
}

RobotomyRequestForm & RobotomyRequestForm::operator=(const RobotomyRequestForm &rhs) {
	if (this == &rhs)
		return (*this);
	Form::operator=(rhs);
	return (*this);
}

void RobotomyRequestForm::execute(const Bureaucrat &exec) const {
    int random;
	if (this->getSigned() == 0)
        std::cout << this->getName() << " can't be execute, it require a signature." << std::endl;
    else if (this->getExecGrade() < exec.getGrade())
        std::cout << exec.getName() << " cannot execute this form, his grade is too low." << std::endl;
    else {
        random = rand() % 2;
        if (random == 0)
            std::cout << "Bzzzz... vmmmm... vmmmmmmm! " << this->_target << " has been robotomized!" << std::endl;
        else
            std::cout << "Robotomizing is a completed failure..." << std::endl;
    }
    return;
}