/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Intern.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/13 13:35:38 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/13 13:35:39 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Intern.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"

Intern::Intern() {
	return;
}

Intern::Intern(const Intern &copy) {
	*this = copy;
}

Intern::~Intern() {
	return;
}

Intern & Intern::operator=(const Intern &rhs) {
	(void)rhs;
	return (*this);
}

Form * makeShrubbery(const std::string target) {
	return (new ShrubberyCreationForm(target));
}

Form * makeRobotomy(const std::string target) {
	return (new RobotomyRequestForm(target));
}

Form * makePresidential(const std::string target) {
	return (new PresidentialPardonForm(target));
}

Form * Intern::makeForm(const std::string &form, const std::string &target) {
	std::string	name[3] = {
			"shrubbery creation",
			"robotomy request",
			"presidential pardon"
	};
	Form * (*maker[3])(std::string) = {
			makeShrubbery,
			makeRobotomy,
			makePresidential
	};

	for (int i = 0; i < 3; i++)
	{
		if (name[i].compare(form) == 0)
		{
			std::cout << "Intern creates " << name[i] << std::endl;
			return (maker[i](target));
		}
	}
	std::cout << "Form couldn't be created, the Intern bring you a PresidentialForm instead" << std::endl;
	return (maker[2](target));
}