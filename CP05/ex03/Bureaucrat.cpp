/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bureaucrat.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/10 17:17:57 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/10 17:17:58 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

# include "Bureaucrat.hpp"

Bureaucrat::Bureaucrat(const std::string name, unsigned int grade) : _name(name), _grade(grade) {
	if (grade < 1)
		throw Bureaucrat::GradeTooHighException();
	else if (grade > 150)
		throw Bureaucrat::GradeTooLowException();

	return;
}

Bureaucrat::Bureaucrat(const Bureaucrat &copy) : _name(copy._name), _grade(copy._grade) {
	return;
}

Bureaucrat::~Bureaucrat() {
	return;
}

Bureaucrat & Bureaucrat::operator=(const Bureaucrat &rhs) {
	if (this == &rhs)
		return (*this);
	this->_name = rhs.getName();
	this->_grade = rhs.getGrade();
	return (*this);
}

std::string Bureaucrat::getName() const {
	return (this->_name);
}

unsigned int Bureaucrat::getGrade() const {
	return (this->_grade);
}

void Bureaucrat::promote() {
	if ((this->_grade - 1) >= 1)
		this->_grade -= 1;
	else
		throw Bureaucrat::GradeTooHighException();
}

void Bureaucrat::demote() {
	if ((this->_grade + 1) <= 150)
		this->_grade += 1;
	else
		throw Bureaucrat::GradeTooLowException();
}

void Bureaucrat::signForm(Form &target) {
	if (target.getSigned())
	{
		std::cout << this->_name << " cannot sign " << target.getName() << " because it's already signed." << std::endl;
		return;
	}
	if (target.getSignGrade() < this->_grade)
	{
		std::cout << this->_name << " cannot sign " << target.getName() << " because his grade is too low." << std::endl;
		return;
	}
	target.setSigned();
	std::cout << this->_name << " signs " << target.getName() << std::endl;
	return;
}

void Bureaucrat::executeForm(const Form &form) {
	if (form.getSigned() == 0)
		std::cout << this->getName() << " cannot execute " << form.getName() << ". It mut be signed first!" << std::endl;
	else if (this->getGrade() > form.getExecGrade())
		std::cout << this->getName() << " cannot execute " << form.getName() << " because his grade is too low." << std::endl;
	else
	{
		std::cout << this->getName() << " executes " << form.getName() << std::endl;
		form.execute(*this);
	}

}

std::ostream &operator<<(std::ostream & o, Bureaucrat const & rhs) {
	o << "<" << rhs.getName() << ">, bureaucrat grade <" << rhs.getGrade() << ">" << std::endl;
	return (o);
}
