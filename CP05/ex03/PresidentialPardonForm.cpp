/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PresidentialPardonForm.cpp                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/13 13:31:50 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/13 13:31:51 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "PresidentialPardonForm.hpp"

PresidentialPardonForm::PresidentialPardonForm(const std::string &target) :
Form("PresidentialPardonForm", 25, 5), _target(target) {
    return;
}

PresidentialPardonForm::PresidentialPardonForm(const PresidentialPardonForm &copy) :
Form(copy), _target(copy._target) {
	return;
}

PresidentialPardonForm::~PresidentialPardonForm() {
	return;
}

PresidentialPardonForm & PresidentialPardonForm::operator=(const PresidentialPardonForm &rhs) {
	if (this == &rhs)
		return (*this);
	Form::operator=(rhs);
	return (*this);
}

void PresidentialPardonForm::execute(const Bureaucrat &exec) const {
    if (this->getSigned() == 0)
        std::cout << this->getName() << " can't be execute, it require a signature." << std::endl;
    else if (this->getExecGrade() < exec.getGrade())
        std::cout << exec.getName() << " cannot execute this form, his grade is too low." << std::endl;
    else
        std::cout << this->_target << " has been forgiven by Zafod Beeblebrox" << std::endl;
}
