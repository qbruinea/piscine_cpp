/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/10 17:19:56 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/10 17:19:57 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.hpp"
#include "Form.hpp"

int main()
{
	try
	{
		Bureaucrat	gerard("Gerard", 8);
		Bureaucrat	didier("Didier", 149);
		Bureaucrat	ginette("Ginette", 2);

		Form		form1("Laisser passer A38", 1);
		Form		form2("Traité de paix", 120);

		std::cout << gerard << didier << ginette;
		gerard.promote();
		std::cout << gerard;
		didier.demote();
		std::cout << didier;
		ginette.promote();
		std::cout << ginette;

		std::cout << form1 << form2;
		ginette.signForm(form1);
		std::cout << form1;
		didier.signForm(form2);
		std::cout << form2;
		form2.beSigned(gerard);
		std::cout << form2;
		didier.signForm(form2);
	}
	catch (const Bureaucrat::GradeTooLowException& e)
	{
		std::cout << e.what() << std::endl;
		return (1);
	}
	catch (const Bureaucrat::GradeTooHighException& e)
	{
		std::cout << e.what() << std::endl;
		return (1);
	}
	catch (const Form::GradeTooLowException& e)
	{
		std::cout << e.what() << std::endl;
		return (1);
	}
	catch (const Form::GradeTooHighException& e)
	{
		std::cout << e.what() << std::endl;
		return (1);
	}

	return (0);
}