/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bureaucrat.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/10 17:18:00 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/10 17:18:01 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUREAUCRAT_HPP
#define BUREAUCRAT_HPP

#include <iostream>

class Bureaucrat {

public:

	Bureaucrat(std::string const name, unsigned int grade);
	Bureaucrat(const Bureaucrat & copy);
	~Bureaucrat();
	Bureaucrat	&operator=(const Bureaucrat & rhs);

	std::string		getName() const;
	unsigned int	getGrade() const;

	void	promote();
	void	demote();

class GradeTooLowException : public std::exception {
public:
	virtual const char * what() const throw() {
		return ("Grade too low");
	}
};

class GradeTooHighException : public std::exception {
public:
	virtual const char * what() const throw() {
		return ("Grade too high");
	}
};

private:

	Bureaucrat();

	std::string		_name;
	unsigned int	_grade;

};

std::ostream &operator<<(std::ostream & o, Bureaucrat const & rhs);

#endif
