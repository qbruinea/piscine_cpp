/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/10 17:18:05 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/10 17:18:06 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.hpp"

int main()
{
	try
	{
		Bureaucrat gerard("Gerard", 8);
		Bureaucrat didier("Didier", 149);
		Bureaucrat ginette("Ginette", 2);

		std::cout << gerard << didier << ginette;
		gerard.promote();
		std::cout << gerard;
		didier.demote();
		std::cout << didier;
		ginette.promote();
		std::cout << ginette;

	}
	catch (const Bureaucrat::GradeTooLowException& e)
	{
		std::cout << e.what() << std::endl;
		return (1);
	}
	catch (const Bureaucrat::GradeTooHighException& e)
	{
		std::cout << e.what() << std::endl;
		return (1);
	}
	return (0);
}