/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bureaucrat.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/10 17:17:57 by qbruinea          #+#    #+#             */
/*   Updated: 2021/04/10 17:17:58 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

# include "Bureaucrat.hpp"

Bureaucrat::Bureaucrat(const std::string name, unsigned int grade) : _name(name), _grade(grade) {
	if (grade < 1)
		throw Bureaucrat::GradeTooHighException();
	else if (grade > 150)
		throw Bureaucrat::GradeTooLowException();

	return;
}

Bureaucrat::Bureaucrat(const Bureaucrat &copy) : _name(copy._name), _grade(copy._grade) {
	return;
}

Bureaucrat::~Bureaucrat() {
	return;
}

Bureaucrat & Bureaucrat::operator=(const Bureaucrat &rhs) {
	if (this == &rhs)
		return (*this);
	this->_name = rhs.getName();
	this->_grade = rhs.getGrade();
	return (*this);
}

std::string Bureaucrat::getName() const {
	return (this->_name);
}

unsigned int Bureaucrat::getGrade() const {
	return (this->_grade);
}

void Bureaucrat::promote() {
	if ((this->_grade - 1) >= 1)
		this->_grade -= 1;
	else
		throw Bureaucrat::GradeTooHighException();
}

void Bureaucrat::demote() {
	if ((this->_grade + 1) <= 150)
		this->_grade += 1;
	else
		throw Bureaucrat::GradeTooLowException();
}

std::ostream &operator<<(std::ostream & o, Bureaucrat const & rhs) {
	o << "<" << rhs.getName() << ">, bureaucrat grade <" << rhs.getGrade() << ">" << std::endl;
	return (o);
}
