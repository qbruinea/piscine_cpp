/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Phonebook.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/17 12:10:17 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/17 12:10:28 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <cstdlib>
#include "Contact.hpp"

static int _print_index_search(Contact list[8], int nb_contact)
{
	std::string entry;

	std::cout << "From who do you want more informations ? Please enter an index : ";
	std::getline(std::cin, entry);
	if (entry.length() != 1)
	{
		std::cout << "Don't fool me, that not a correct index. Returning to menu." << std::endl;
		return (0);
	}
	int index = atoi(entry.c_str());
	if (index > nb_contact || index < 0)
	{
		std::cout << "Don't fool me, that not a correct index. Returning to menu." << std::endl;
		return (0);
	}
	list[index].print_contact();
	return (1);
}

static void _print_first_search(std::string str)
{
	if (str.length() > 10)
	{
		str.resize(9);
		str.append(".");
	}
	else
		for (int i = 10; i - str.length() != 0; i--)
			std::cout << " ";
	std::cout << str;
}

static int 	_search(Contact list[8])
{
	int i = 0;

	std::cout << "     index|    prenom|       nom|    pseudo" << std::endl;
	std::cout << "-------------------------------------------" << std::endl;
	while (!list[i].is_empty() && i < 8)
	{
		std::cout << "         " << i << "|";
		_print_first_search(list[i].getFirstName());
		std::cout << "|";
		_print_first_search(list[i].getLastName());
		std::cout << "|";
		_print_first_search(list[i].getNickName());
		std::cout << std::endl;
		i++;
	}
	std::cout << "-------------------------------------------" << std::endl;
	if (i == 0)
	{
		std::cout << "Phonebook is empty. Returning to menu." << std::endl;
		return (0);
	}
	else if (i > 0)
	{
		if (_print_index_search(list, i) == 0)
			return (0);
	}
	return (0);
}

static int 	_add(Contact list[8])
{
	int	i = 0;
	while (!list[i].is_empty() && i < 8)
		i++;
	if (i == 8)
	{
		std::cout << "Phonebook is full ! " << std::endl;
		return (0);
	}
	list[i].init_contact();
	return (0);
}

static void	_end(void)
{
	std::cout << "Exiting phonebook" << std::endl;
	exit (0);
}

static int 	_menu(void)
{
	std::string entry;
	std::cout << "Welcome to your phonebook : type ADD / SEARCH or EXIT" << std::endl;
	std::getline(std::cin, entry);
	if (std::cin.eof())
		return (3);
	if (!entry.compare("ADD"))
		return (1);
	if (!entry.compare("SEARCH"))
		return (2);
	if (!entry.compare("EXIT"))
		return (3);
	return (0);
}

int main(void)
{
	Contact list[8];
	int 	cmd;

	cmd = 0;
	while (1)
	{
		if (cmd == 0)
			cmd = _menu();
		else if (cmd == 1)
			cmd = _add(list);
		else if (cmd == 2)
			cmd = _search(list);
		else if (cmd == 3)
			_end();
	}
	return (0);
}
