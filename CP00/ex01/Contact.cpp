/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Contact.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbruinea <qbruinea@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/17 12:09:39 by qbruinea          #+#    #+#             */
/*   Updated: 2021/03/17 12:09:48 by qbruinea         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Contact.hpp"

Contact::Contact(void) {
	_empty = true;
	return;
}

Contact::~Contact(void) {
	return;
}

void Contact::setFirstName(const std::string &first_name) {
	this->_first_name = first_name;
	return;
}

void Contact::setLastName(const std::string &last_name) {
	this->_last_name = last_name;
	return;
}

void Contact::setNickname(const std::string &nickname) {
	this->_nickname = nickname;
	return;
}

void Contact::setLogin(const std::string &login) {
	this->_login = login;
	return;
}

void Contact::setPostalAddress(const std::string &postal_address) {
	this->_postal_address = postal_address;
	return;
}

void Contact::setEmailAddress(const std::string &email_address) {
	this->_email_address = email_address;
	return;
}

void Contact::setPhoneNumber(const std::string &phone_number) {
	this->_phone_number = phone_number;
	return;
}

void Contact::setBirthdayDate(const std::string &birthday_date) {
	this->_birthday_date = birthday_date;
	return;
}

void Contact::setFavoriteMeal(const std::string &favorite_meal) {
	this->_favorite_meal = favorite_meal;
	return;
}

void Contact::setUnderwearColor(const std::string &underwear_color) {
	this->_underwear_color = underwear_color;
	return;
}

void Contact::setDarkestSecret(const std::string &darkest_secret) {
	this->_darkest_secret = darkest_secret;
	return;
}

const	std::string &Contact::getFirstName() {
	return (this->_first_name);
}

const	std::string	&Contact::getLastName() {
	return (this->_last_name);
}

const	std::string &Contact::getNickName() {
	return (this->_nickname);
}

const	std::string &Contact::getLogin() {
	return (this->_login);
}

const	std::string &Contact::getPostalAddress() {
	return (this->_postal_address);
}

const	std::string &Contact::getEmailAddress() {
	return (this->_email_address);
}

const	std::string &Contact::getPhoneNumber() {
	return (this->_phone_number);
}

const	std::string &Contact::getBirthdayDate() {
	return (this->_birthday_date);
}

const	std::string &Contact::getFavoriteMeal() {
	return (this->_favorite_meal);
}

const	std::string &Contact::getUnderwearColor() {
	return (this->_underwear_color);
}

const	std::string &Contact::getDarkestSecret() {
	return (this->_darkest_secret);
}

bool Contact::is_empty() {
	return (this->_empty);
}

void	Contact::init_contact() {
	std::string tab[11] = {
			"First name : ",
			"Last name : ",
			"Nickname : ",
			"Login : ",
			"Postal address : ",
			"Email address : ",
			"Phone number : ",
			"Birthday date : ",
			"Favorite meal : ",
			"Underwear color : ",
			"Darkest secret : "
	};

	std::string param;
	for (int i = 0; i < 11; i++)
	{
		std::cout << tab[i];
		std::getline(std::cin, param);
		if (i == 0)
			this->setFirstName(param);
		if (i == 1)
			this->setLastName(param);
		if (i == 2)
			this->setNickname(param);
		if (i == 3)
			this->setLogin(param);
		if (i == 4)
			this->setPostalAddress(param);
		if (i == 5)
			this->setEmailAddress(param);
		if (i == 6)
			this->setPhoneNumber(param);
		if (i == 7)
			this->setBirthdayDate(param);
		if (i == 8)
			this->setFavoriteMeal(param);
		if (i == 9)
			this->setUnderwearColor(param);
		if (i == 10)
			this->setDarkestSecret(param);
	}
	this->_empty = false;
	std::cout << "Contact successfully added !" << std::endl;
}

void	Contact::print_contact() {
	std::string tab[11] = {
			"First name : ",
			"Last name : ",
			"Nickname : ",
			"Login : ",
			"Postal address : ",
			"Email address : ",
			"Phone number : ",
			"Birthday date : ",
			"Favorite meal : ",
			"Underwear color : ",
			"Darkest secret : "
	};
	std::string info[11] = {
			this->getFirstName(),
			this->getLastName(),
			this->getNickName(),
			this->getLogin(),
			this->getPostalAddress(),
			this->getEmailAddress(),
			this->getPhoneNumber(),
			this->getBirthdayDate(),
			this->getFavoriteMeal(),
			this->getUnderwearColor(),
			this->getDarkestSecret()
	};
	
	for (int i = 0; i < 11; i++)
		std::cout << tab[i] << info[i] << std::endl;
}
